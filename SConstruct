import os.path

opts = Options('scache.conf')
### These options are global defaults.
### If you want to change your options locally,
### give them to SCons on the command line, or
### edit scache.conf. Don't edit them here.
opts.AddOptions(
    ('CC', 'Set the C compiler to use'),
    ('AS', 'Set the NASM executable name'),
    ('LINK', 'Set the linker to use'),
    ('CFLAGS', 'Set the C compiler flags', '-ffreestanding -m32 -O3 -Wall -std=c99 -pedantic'),
    ('ASFLAGS', 'Set the assembler flags', ''),
    ('LINKFLAGS', 'Set the linker flags', '-ffreestanding -nostdlib -Wl,--gc-sections,-m,elf_i386'),
    ('BUILDDIR', 'Set the sub-directory to put object files in', 'build'),
    BoolOption('verbose', 'Show full commands during the build process', False),
)

env = Environment(options = opts, tools=['default', 'nasm'])
Help(opts.GenerateHelpText(env))
opts.Save('scache.conf', env)

class OurTests:
    def check_nasm32(context):
        """check whether nasm uses -f elf or -f elf32"""
        context.Message('Checking for nasm -f elf32... ')
        context.env['ASFLAGS'] = '-f elf32'
        result = context.TryCompile('', '.asm')
        context.Result(result)
        return result

    def check_stack_prot(context):
        """check whether gcc supports -fno-stack-protector"""
        context.Message('Checking for -fno-stack-protector... ')
        context.env['CFLAGS'] = '-fno-stack-protector'
        result = context.TryCompile('', '.c')
        context.Result(result)
        return result

old_env = env.Clone()

if not env.GetOption('clean'):
    ### RUN TESTS AND CONFIGURE ENVIRONMENT ###
    conf = Configure(env, custom_tests = OurTests.__dict__)
    
    # Does NASM want '-f elf' or '-f elf32' ?
    if conf.check_nasm32():
        env['ASFLAGS'] = old_env['ASFLAGS'] + ' -f elf32'
    else:
        env['ASFLAGS'] = old_env['ASFLAGS'] + ' -f elf'
    
    # If the compiler has -fno-stack-protector, we need to disable
    # it to stop gcc inserting stack protection code (which doesn't
    # work in a kernel)
    if conf.check_stack_prot():
        env['CFLAGS'] = old_env['CFLAGS'] + ' -fno-stack-protector'
    else:
        env['CFLAGS'] = old_env['CFLAGS']

    env = conf.Finish()

# set nice pretty messages
if not env['verbose']:
    env['CCCOMSTR'] =     '     Compiling [32m$TARGET[0m'
    env['ASCOMSTR'] =     '    Assembling [32m$TARGET[0m'
    env['LINKCOMSTR'] =   '       Linking [32m$TARGET[0m'
    env['ARCOMSTR'] =     '     Archiving [32m$TARGET[0m'
    env['RANLIBCOMSTR'] = '      Indexing [32m$TARGET[0m'
    env['NMCOMSTR'] =     '  Creating map [32m$TARGET[0m'

SConscript('SConscript',
    exports = ['env'],
    build_dir = env['BUILDDIR'],
    duplicate = 0)

# clean config data
env.Clean('.', ['config.log', '.sconf_temp'])
