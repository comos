#ifndef CHARDEV_H
#define CHARDEV_H

// Character device management

#include "stdint.h"

enum {
    MAX_CHARDEV_MAJOR = 255,
};

struct chardev;

struct chardev_functab {
    ssize_t (* read)(struct chardev *, void *buf, size_t len);
    ssize_t (* write)(struct chardev *, const void *buf, size_t len);
};

struct chardev {
    struct chardev *prev, *next;
    struct chardev_functab *functab;
    char major, minor;      // major/minor device numbers
};

// initialize the character device manager
void chardev_init(void);

ssize_t chardev_read(struct chardev *, void *buf, size_t len);
ssize_t chardev_write(struct chardev *, const void *buf, size_t len);

// character device registration - after registration,
// the device can be referenced by its major/minor device numbers.
int chardev_register(struct chardev *cdev);
int chardev_unregister(struct chardev *cdev);

// get a character device by its major/minor device numbers
struct chardev *chardev_get(int major, int minor);

#endif
