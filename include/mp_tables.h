#ifndef MP_TABLES_H
#define MP_TABLES_H

#include "stdint.h"

struct mp_processor
{
    uint8_t  entry_type,         /* 0 = MP Processor Entry */
         local_apic_id,
         local_apic_version,
             cpu_flags;
    uint32_t cpu_signature;
    uint32_t feature_flags;
    uint32_t reserved[2];
};

struct mp_bus
{
    uint8_t  entry_type;         /* 1 = MP BUS Entry */
    uint8_t  bus_id;             /* bus id (used for IO-APIC Redirection Tables) */
    uint8_t  bus_type_string[7]; /* bus 6-byte ascii string (don't forget the zero-termination byte) */
};

struct mp_io_apic
{
    uint8_t  entry_type;         /* 2 = MP IO APIC Entry */
    uint8_t  id;                 /* IO APIC ID (used for IO-APIC Redirection Tables) */
    uint8_t  version;
    uint8_t  flags;
    uint32_t address;            /* Address of IO APIC base */
};

struct mp_io_interrupt
{
    uint8_t  entry_type;        /* 3 = MP IO Interrupt Entry */
    uint8_t  int_type;          /* edge/level triggered etc... */
    uint16_t flags;     
    uint8_t  src_id;            /* source bus id */
    uint8_t  src_io;            /* source interrupt */
    uint8_t  dst_id;            /* destination bus id */
    uint8_t  dst_io;            /* destination interrupt */
};

struct mp_local_interrupt
{
    uint8_t  entry_type;        /* 3 = MP Local Interrupt Entry */
    uint8_t  int_type;          /* edge/level triggered etc... */
    uint16_t flags;     
    uint8_t  src_id;            /* source bus id */
    uint8_t  src_io;            /* source interrupt */
    uint8_t  dst_id;            /* destination bus id */
    uint8_t  dst_io;            /* destination interrupt */
};

struct mp_counts
{
    uint32_t processor_count,
             bus_count,
             io_apic_count,
             io_interrupt_count,
             local_interrupt_count;
};

void parse_mp_tables(void);

#endif
