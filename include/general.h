#ifndef GENERAL_H
#define GENERAL_H

#include "stddef.h"
#include "stdint.h"

#define kernel_codename "COMOS"
#define kernel_version "0.01"

void panic_internal(const char *file, int line, const char *fmt, ...);
#define panic(...) panic_internal(__FILE__, __LINE__, __VA_ARGS__)

// these are defined in math.c
void init_sse(void);
void init_fpu(void);

static inline void cli(void)
{
    asm volatile ("cli");
}

static inline void sti(void)
{
    asm volatile ("sti");
}

static inline void hlt(void)
{
    asm volatile ("hlt");
}

/* Some standard functions */
char * strtok( char * s1, const char * s2 );
//int strcmp(char *str1, char *str2);
char *strcpy(char *dest, const char *src);
char *strcat(char *dest, const char *src);
//int strlen(char *src);

#endif
