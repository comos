#ifndef CPU_H
#define CPU_H

#include "stdint.h"

void apic_init(void);
uint8_t get_cpu_id(void);
void enable_local_apic(void);

#endif
