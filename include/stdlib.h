#ifndef STDLIB_H
#define STDLIB_H

#include "stdint.h"
#include "stdarg.h"

// Traditionally, these are split up into
// string.h etc.

// these are defined in libc-asm.asm
void *memcpy(void *dest, const void *src, size_t count);
void *memmove(void *dest, const void *src, size_t count);
void *memset(void *dest, int val, size_t count);
int memcmp(const void *src1, const void *src2, size_t count);
wchar_t *wmemset(wchar_t *dest, wchar_t val, size_t count);
size_t strlen(const char *str);

// these are defined in sprintf.c
int vsnprintf(char *buffer, size_t n, const char *fmt, va_list ap);
int snprintf(char *str, size_t n, const char *fmt, ...);
int vsprintf(char *str, const char *fmt, va_list ap);
int sprintf(char *str, const char *fmt, ...);

// these are defined in general.c
int strcmp(const char *str1, const char *str2);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);

// these are defined in malloc.c
void *malloc(size_t size);
void *realloc(void *ptr, size_t new_size);
void free(void *ptr);

#endif

