#ifndef ALIGN_H
#define ALIGN_H

// Functions related to alignment and rounding etc.

// return a/b rounded up
// It's a pity that inline functions have to be static, and implemented
// in headers.
static inline unsigned long uldivru(unsigned long a, unsigned long b)
{
    unsigned long d;
    d = a / b;
    if (a % b)
        d++;
    return d;
}

static inline unsigned long long ulldivru(unsigned long long a, unsigned long long b)
{
    unsigned long long d;
    d = a / b;
    if (a % b)
        d++;
    return d;
}

// return log base 2 of v
static inline unsigned int ilog2(unsigned int v)
{
    static const unsigned int b[] = {0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000};
    static const unsigned int S[] = {1, 2, 4, 8, 16};
    int i;
    register unsigned int r = 0; // result goes here
    for (i=4; i>=0; i--){
        if (v & b[i]){
            v >>= S[i];
            r |= S[i];
        }
    }
    return r;
}

// return log base 2 of v, rounded up
static inline unsigned int ilog2up(unsigned int v)
{
    // do it the nasty way
    unsigned int log = ilog2(v);
    if (v & ((1 << log) - 1))
        log++;
    return log;
}

// return 'value' rounded up to nearest 'alignment' multiple
// hopefully, since these are inline, cc will optimize the division
// and modulus for shifts and ands.
static inline unsigned long alignup(unsigned long value, unsigned long alignment)
{
    return uldivru(value, alignment) * alignment;
}

// return 'value' rounded down to nearest 'alignment' multiple
static inline unsigned long aligndn(unsigned long value, unsigned long alignment)
{
    return (value / alignment) * alignment;
}

#endif
