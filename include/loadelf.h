#ifndef LOADELF_H
#define LOADELF_H

#include "stdint.h"

int load_elf_module(void *start_addr, intptr_t phys_addr, size_t length);

#endif
