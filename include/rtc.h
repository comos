#ifndef RTC_H
#define RTC_H

#include "stdlib.h"

struct time_s
{
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t day_of_week;
    uint8_t day_of_month;
    uint8_t month;
    uint16_t year;
} time;

/* these should get moved to a NET header later on */
#define hs2net_16(val) (uint16_t)  ((((val) >> 8) & 0xFF) | (((val) & 0xFF) << 8))
#define net2hs_16(val) hs2net_16(val)

#define hs2net_32(val) (uint32_t) (((((val) & 0xFF) << 24) | ((val) >> 24) & 0xFF) | \
                                    (((val) & 0xFF0000) >> 8) | (((val) & 0xFF00) << 8))
#define net2hs_32(val) hs2net_32(val)

#define bcd2bin(x) (((x >> 4) * 10) + (x & 0x0F))

void init_rtc(void);

#endif

