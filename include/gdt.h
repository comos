#ifndef GDT_H
#define GDT_H

#include "stdint.h"

// Segment selectors - these correspond to entries
// in the GDT (in gdt.c)
enum {
    SEG_DPL0_CODE = 0x08,   // kernel-level code
    SEG_DPL0_DATA = 0x10,   // kernel-level data
    SEG_DPL3_CODE = 0x1B,   // user-level code
    SEG_DPL3_DATA = 0x23,   // user-level data
    SEG_DPL3_TSS = 0x2B,    // user-level TSS
};

// The structure of the Task State Segment
struct tss {
    uint16_t prev, res0;
    uint32_t esp0;
    uint16_t ss0, res1;
    uint32_t esp1;
    uint16_t ss1, res2;
    uint32_t esp2;
    uint16_t ss2, res3;
    uint32_t cr3, eip, eflags, eax, ecx, edx, ebx, esp, ebp, esi, edi;
    uint16_t es, res4, cs, res5, ss, res6, ds, res7, fs, res8, gs, res9;
    uint16_t ldt, res10, debug_trap, iomap_base;
};

// Main initialization function for the GDT
void gdt_init(void);

#endif
