#ifndef PORTIO_H
#define PORTIO_H

#include "stddef.h"
#include "stdint.h"

// input/output with x86 ports

static inline void outb(int port, uint8_t value)
{
    asm volatile ("outb %%al,%%dx" :: "a" (value), "d" (port));
}

static inline void outw(int port, uint16_t value)
{
    asm volatile ("outw %%ax,%%dx" :: "a" (value), "d" (port));
}

static inline void outl(int port, uint32_t value)
{
    asm volatile ("outl %%eax,%%dx" :: "a" (value), "d" (port));
}

static inline uint8_t inb(int port)
{
    uint8_t value;
    asm volatile ("inb %%dx,%%al" : "=a" (value) : "d" (port));
    return value;
}

static inline uint16_t inw(int port)
{
    uint16_t value;
    asm volatile ("inw %%dx,%%ax" : "=a" (value) : "d" (port));
    return value;
}

static inline uint32_t inl(int port)
{
    uint32_t value;
    asm volatile ("inl %%dx,%%eax" : "=a" (value) : "d" (port));
    return value;
}


#endif
