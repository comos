#ifndef MULTIBOOT_H
#define MULTIBOOT_H

// magic numbers
#define MB_MBH_MAGIC 0x1BADB002 // magic number in multiboot header
#define MB_BOOT_MAGIC 0x2BADB002 // magic number in eax at startup

// mb_info flags
#define MBI_MEM_XXX 0x01
#define MBI_BOOT_DEVICE 0x02
#define MBI_CMDLINE 0x04
#define MBI_MODS_XXX 0x08
#define MBI_SYMS 0x30
#define MBI_MMAP_XXX 0x40
#define MBI_DRIVES_XXX 0x80
#define MBI_CONFIG_TABLE 0x100
#define MBI_BOOT_LOADER_NAME 0x200
#define MBI_APM_TABLE 0x400
#define MBI_VBE_XXX 0x800

struct mb_info {
    unsigned long flags, mem_lower, mem_upper,
        boot_device, cmd_line, mods_count, mods_addr,
        syms[4], mmap_length, mmap_addr, drives_length,
        drives_addr, config_table, boot_loader_name,
        apm_table, vbe_control_info, vbe_mode_info,
        vbe_mode, vbe_interface_seg, vbe_interface_off,
        vbe_interface_len;
};

struct mb_module {
    unsigned long mod_start, mod_end, string, reserved;
};

struct mb_mmap {
    unsigned long size;
    unsigned long long base_addr, length;
    unsigned long type;
};

struct mb_drive {
    unsigned long size;
    unsigned char drive_number, drive_mode;
    unsigned short drive_cylinders;
    unsigned char drive_heads, drive_sectors;
    unsigned short drive_ports[1];
};

#endif

