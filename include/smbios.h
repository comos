#ifndef SMBIOS_H
#define SMBIOS_H

#include "stdlib.h"

struct sm_memdev_s
{
    uint16_t memory_handle;
    uint16_t error_handle;
    uint16_t total_width;
    uint16_t data_width;
    uint16_t size;
    uint8_t  form_factor;
    uint8_t  device_set;
    uint8_t  device_locator;
    uint8_t  bank_locator;
    uint8_t  type;
    uint16_t type_detail;
    uint16_t speed;
    uint8_t  manufacturer;
    uint8_t  serial;
    uint8_t  asset;
    uint8_t  part;
    uint8_t  attributes;
    
    char *   device_locator_s;
    char *   bank_locator_s;
    char *   manufacturer_s;
    char *   serial_s;
    char *   asset_s;
    char *   part_s;
} sm_memdev[64];

struct sm_cpu_s
{
    uint8_t  socket;
    uint8_t  type;
    uint8_t  family;
    uint8_t  manufacturer;
    uint32_t id;
    uint8_t  version;
    uint8_t  voltage;
    uint16_t external_clock;
    uint16_t max_speed;
    uint16_t current_speed;
    uint8_t  status;
    uint8_t  upgrade;
    uint16_t L1_cache_handle;
    uint16_t L2_cache_handle;
    uint16_t L3_cache_handle;
    uint8_t  serial;
    uint8_t  asset;
    uint8_t  part;
    uint8_t  core_count;
    uint8_t  core_enabled;
    uint8_t  thread_count;
    uint16_t characteristics;
    uint16_t ext_family;
    
    char *   socket_s;
    char *   manufacturer_s;
    char *   version_s;
    char *   serial_s;
    char *   asset_s;
    char *   part_s;
} sm_cpu[64]; /* yeah, that's right... 64 CPU's baby!!! */


struct sm_chas_s
{
    uint8_t  manufacturer;
    uint8_t  type;
    uint8_t  version;
    uint8_t  serial;
    uint8_t  asset;
    uint8_t  boot_state;
    uint8_t  power_state;
    uint8_t  thermal_state;
    uint8_t  security_state;
    uint32_t oem_defined;
    uint8_t  height;
    uint8_t  power_cord_count;
    uint8_t  contained_element_count;
    uint8_t  contained_record_length;
    uint8_t  contained_elements[256][256]; /* count * length */
    
    char *   manufacturer_s;
    char *   version_s;
    char *   serial_s;
    char *   asset_s;
} sm_chas[4];

struct sm_mod_s
{
    uint8_t  manufacturer;
    uint8_t  product;
    uint8_t  version;
    uint8_t  serial;
    uint8_t  asset;
    uint8_t  family;
    uint8_t  feature_flags;
    uint8_t  location;
    uint16_t chassis_handle;
    uint8_t  board_type;
    uint8_t  num_contained_objects;
    uint16_t contained_objects[256];    /* max */
    
    char *   manufacturer_s;
    char *   product_s;
    char *   version_s;
    char *   serial_s;
    char *   asset_s;
    char *   location_s;
} sm_mod[8];

struct sm_sys_s
{
    uint8_t  manufacturer;
    uint8_t  product;
    uint8_t  version;
    uint8_t  serial;
    uint8_t  uuid[16];
    uint8_t  wakeup_type;
    uint8_t  sku;
    uint8_t  family;
    
    char *   manufacturer_s;
    char *   product_s;
    char *   version_s;
    char *   serial_s;
    char *   sku_s;
    char *   family_s;
} sm_sys[4];

struct sm_bios_s
{
    uint8_t  vendor;
    uint8_t  version;
    uint16_t starting_address_segment;
    uint8_t  release_date;
    uint8_t  rom_size;
    uint8_t  characteristics[8];
    uint8_t  extension[2];
    uint8_t  major_release;
    uint8_t  minor_release;
    uint8_t  firmware_major_release;
    uint8_t  firmware_minor_release;
    
    char *   vendor_s;
    char *   version_s;
    char *   release_date_s;
} sm_bios;

struct sm_hdr_s
{
    uint8_t  type;
    uint8_t  length;
    uint16_t handle;
} sm_hdr;

struct sm_entry_s
{
    uint8_t  anchor_string[4];
    uint8_t  checksum;
    uint8_t  length;
    uint8_t  major_version;
    uint8_t  minor_version;
    uint16_t max_struct_size;
    uint8_t  revision;
    uint8_t  formatted_area[5];
    uint8_t  interm_anchor_string[5];
    uint8_t  interm_checksum;
    uint16_t struct_table_length;
    uint32_t struct_table_address;
    uint16_t num_smbios_structures;
    uint8_t  bcd_revision;
} sm_eps;

uint8_t sm_bios_count,
        sm_sys_count,
        sm_mod_count,
        sm_chas_count,
        sm_cpu_count,
        sm_memdev_count;

void parse_smbios(void);

#endif



