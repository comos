#ifndef SHELL_H
#define SHELL_H

#include "stdint.h"

typedef void (*fnList)(char *);
void kshell_task(void);
void kshell_lookup(char * buffer);
void kshell_function(char *fname, fnList fFunctionh, char *fdesc);

void kshell_add_show_child(char *fname, fnList fFunction);

void kshell_keypress(uint8_t key);	

//Shell functions
void shell_ver(char * buffer);
void shell_cmds(char * buffer);
void shell_clear(char * buffer);
void shell_ps(char * buffer);
void shell_show(char * buffer);

void shell_cpuid(char * buffer);
void shell_system_info(char * buffer);
void shell_interrupts(char * buffer);

#endif
