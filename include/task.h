#ifndef TASK_H
#define TASK_H

#include "stdint.h"
#include "stdbool.h"
#include "interrupt.h"
#include "mm/region.h"

enum task_state {
    MAX_TASKS = 256,

    TASK_NAME_LEN = 32,

    TASK_NULL = 0,
    TASK_INIT = 1,
    TASK_READY = 2,
    TASK_KILLED = 7,

    PRIO_CRITICAL = 1,
    PRIO_HIGH = 3,
    PRIO_NORMAL = 5,
    PRIO_LOW = 7,
    PRIO_VERYLOW = 9,
    PRIO_SLEEPING = 10,
};

extern struct task_s
{
    uint32_t *stack_base;       // bottom of stack
    struct addr_space *aspace;  // address space that task runs in
    char name[TASK_NAME_LEN];   // ASCII name
    enum task_state state;      // Task STATE
    
    struct pusha_struct gpr;    // general purpose registers
    uint32_t eip, eflags;
    
    uint32_t priority;          // priority (1-100)
    uint32_t time_till_exec;    // timer int's until next execution
    
} task[MAX_TASKS];

uint32_t active_id;
uint32_t num_active_tasks, num_sleeping_tasks;
bool MULTITASKING;

typedef void (* entry_point_t)(void);

// these are defined in task.c
void switch_task(struct interrupt_stack *s);
void init_multitasking(void);
struct task_s *create_task(struct addr_space *aspace, entry_point_t entry_pt, char name[32], uint32_t priority);
void kill_task(uint32_t id);
void change_task_priority(uint32_t id, uint32_t priority);
#endif
