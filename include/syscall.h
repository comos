#ifndef SYSCALL_H
#define SYSCALL_H

// Set up and manage system calls

#include "stdint.h"

enum {
    SYSCALL_COUNT = 32, // we are expecting more than 32
};

struct syscall_params {
    void *stack;
    uint32_t a, b, c, d, e;
};

typedef int (* syscall_t)(struct syscall_params *);

// initialise the syscall system
void syscall_init(void);

// register a system call
int register_syscall(int syscall_number, syscall_t);

#endif
