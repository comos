#ifndef CONSOLE_H
#define CONSOLE_H

#include "stdarg.h"
#include "serial.h"
#include "stdint.h"

#define DEBUG // trace messages will be printed if this is defined

#ifdef DEBUG
#define trace(...) do { \
    console_printf(get_vterm(0), __FILE__ ":%d: (%s): ", __LINE__, __func__); \
    console_printf(get_vterm(0), __VA_ARGS__); \
} while (0)
#else
#define trace(...) do {} while (0)
#endif

// Available terminal colours
enum {
    COLOUR_BLACK = 0,
    COLOUR_BLUE,
    COLOUR_GREEN,
    COLOUR_CYAN,
    COLOUR_RED,
    COLOUR_MAGENTA,
    COLOUR_BROWN,
    COLOUR_WHITE,
    COLOUR_GREY,
    COLOUR_BRIGHT_BLUE,
    COLOUR_BRIGHT_GREEN,
    COLOUR_BRIGHT_CYAN,
    COLOUR_BRIGHT_RED,
    COLOUR_BRIGHT_MAGENTA,
    COLOUR_BRIGHT_YELLOW,
    COLOUR_BRIGHT_WHITE,
    // Repeat the same constants again, but for Americans.
    COLOR_BLACK = 0,
    COLOR_BLUE,
    COLOR_GREEN,
    COLOR_CYAN,
    COLOR_RED,
    COLOR_MAGENTA,
    COLOR_BROWN,
    COLOR_WHITE,
    COLOR_GREY,
    COLOR_BRIGHT_BLUE,
    COLOR_BRIGHT_GREEN,
    COLOR_BRIGHT_CYAN,
    COLOR_BRIGHT_RED,
    COLOR_BRIGHT_MAGENTA,
    COLOR_BRIGHT_YELLOW,
    COLOR_BRIGHT_WHITE
};

// vterm structure - forward declaration
// users don't need to know what's inside the structure.
struct vterm;

void console_init(void);
void console_change_vram(uint16_t *new_video_ram);      // change the VRAM address to use (during bootup only)
struct vterm *get_vterm(int n);                         // get a pointer to virtual terminal n
void switch_vterm(struct vterm *new_vterm);             // switch to new_vterm
void console_clear(struct vterm *vt);                   // clear the console and reset the cursor to the origin
void console_putchar(struct vterm *vt, char c);         // write a single character
void console_puts(struct vterm *vt, const char *str);   // write a string
void console_reset_colour(struct vterm *vt);            // reset colours to default
void console_set_fcolour(struct vterm *vt, int new_fcolour);    // set foreground colour
void console_set_fbcolour(struct vterm *vt, int new_fcolour, int new_bcolour);    // set foreground and background colours
void console_printf(struct vterm *vt, const char *fmt, ...);    // printf-like output
void console_vprintf(struct vterm *vt, const char *fmt, va_list ap);
void console_serial_enable(struct vterm *vt, struct serial *);  // enable output to a serial port, as well as the screen (a bit hackish)

#endif


