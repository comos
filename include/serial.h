#ifndef SERIAL_H
#define SERIAL_H

#include "stdbool.h"

struct serial {
    bool initialized;
    int port;
    int irq;
};

// Initialize a serial port. Return 0=ok, 1=failure
int serial_init(struct serial *self, int com_number, unsigned long baud); // initialize serial port com_number (com1, com2...) at baudrate 'baud'

void serial_puts(struct serial *self, const char *str); // send 'str' out the serial port
void serial_putchar(struct serial *self, char ch); // send a character out the serial port

extern struct serial com1, com2, com3, com4;

#endif
