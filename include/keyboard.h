#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "general.h"

enum {
    // I'm not sure what these are.
    K_VTERM_1 = 236,
    K_VTERM_2 = 237,
    K_VTERM_3 = 238,
    K_VTERM_4 = 239,

    K_LEFT = 240,
    K_UP = 241,
    K_DOWN = 242,
    K_RIGHT = 243,

    K_F1 = 244,
    K_F2 = 245,
    K_F3 = 246,
    K_F4 = 247,
    K_F5 = 248,
    K_F6 = 249,
    K_F7 = 250,
    K_F8 = 251,
    K_F9 = 252,
    K_F10 = 253,
    K_F11 = 254,
    K_F12 = 255,

    // Keyboard states
    KBD_SHIFT_STATE = 0x01,
    KBD_CTRL_STATE = 0x02,
    KBD_ALT_STATE = 0x04,
    KBD_CAPSLOCK_STATE = 0x10,
    KBD_NUMLOCK_STATE = 0x20,
    KBD_SCROLLLOCK_STATE = 0x40,
};

typedef void (*kbListener)(uint8_t);

void keyboard_register_listener(kbListener nlist);
void keyboard_init(void);
void kb_set_leds(int led_states); // set keyboard LEDs

#endif
