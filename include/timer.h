#ifndef TIMER_H
#define TIMER_H

typedef long long tick_t;

// timer initialization - call once, during bootup
void timer_init(void);

// get current timer value
tick_t get_tick(void);

// set frequency of the timer (used by the task_mgr)
void set_frequency(unsigned long new_freq);

// get current cpu timestamp (64-bit return)
unsigned long long int read_cpu_timestamp(void);

#endif

