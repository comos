#ifndef CPUSTRUCT_H
#define CPUSTRUCT_H

#include "stdint.h"

// data structure created with
// the 'pusha' instruction
struct pusha_struct {
    uint32_t edi, esi, ebp, esp,
      ebx, edx, ecx, eax; // 0x10
};

#endif


