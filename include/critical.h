#ifndef CRITICAL_H
#define CRITICAL_H

// Code for critical sections and SMP synchronization

typedef int critical_state_t;

critical_state_t critical_enter(void);      // enters a critical section and returns the previous critical section state
void critical_exit(critical_state_t state); // leaves a critical section - restores critical section state */

/* critical section example:
     int critical_state = critical_enter();
     do_critical_stuff();
     critical_exit(critical_state);
*/

void memory_barrier(void);  /* memory barrier */

#endif
