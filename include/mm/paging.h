#ifndef MM_PAGING
#define MM_PAGING

#include "stddef.h"
#include "stdint.h"
#include "mm/mm.h"

// Types for page directory entries and
// page table entries (they're almost the same format,
// but it's clearer if we use two different identifiers)
typedef uint32_t pte_t, pde_t;

// page table/directory format
// as defined in the documentation for the CPU
// The page table (PTE_*) and page directory (PDE_*) flags
// are subtly different.
enum {
    PTE_PRESENT        = 1 << 0,    // page is valid
    PTE_WRITABLE       = 1 << 1,    // write access bit
    PTE_USER           = 1 << 2,    // user (ring1 - ring3) accessible
    PTE_WRITE_THROUGH  = 1 << 3,
    PTE_CACHE_DISABLED = 1 << 4,
    PTE_ACCESSED       = 1 << 5,    // set by the CPU when page is accessed
    PTE_DIRTY          = 1 << 6,    // set by the CPU when page is written to
    PTE_PAT            = 1 << 7,
    PTE_GLOBAL         = 1 << 8,
    PTE_ADDRESS = (int) 0xFFFFF000,

    PDE_PRESENT        = 1 << 0,
    PDE_WRITABLE       = 1 << 1,
    PDE_USER           = 1 << 2,
    PDE_WRITE_THROUGH  = 1 << 3,
    PDE_CACHE_DISABLED = 1 << 4,
    PDE_ACCESSED       = 1 << 5,
    PDE_PAGESIZE       = 1 << 7,
    PDE_GLOBAL         = 1 << 8,
    PDE_ADDRESS = (int) 0xFFFFF000,
};

// Page table and directory locations, based on the
// reflexive mapping technique.
static pte_t *const PAGE_TAB = (pte_t *) 0xFFC00000;
static pde_t *const PAGE_DIR = (pde_t *) 0xFFFFF000;

struct pagedir {
    struct pagedir *prev, *next; // linked list (so we can update all page directories at once)
    intptr_t phys_addr; // physical address of page directory
    pde_t *virt_addr; // kernel-virtual address of page directory
};

// Turn on reflexive paging, i.e. the page directory is treated as a page table
// and we can gain access to all our current page tables and the page directory
// at 0xFFC00000 - 0xFFFFFFFF pretty much for free.
void set_up_reflexive_paging(void);

// functions to get/set cr2 and cr3
// cr2 contains the virtual address of the last page fault.
// cr3 contains the address of the page directory.

static inline uint32_t get_cr2(void)
{
    uint32_t old_cr3;
    asm volatile ("movl %%cr2,%%eax" : "=a" (old_cr3));
    return old_cr3;
}

static inline uint32_t get_cr3(void)
{
    uint32_t old_cr3;
    asm volatile ("movl %%cr3,%%eax" : "=a" (old_cr3));
    return old_cr3;
}

static inline void set_cr3(uint32_t new_cr3)
{
    asm volatile ("movl %%eax,%%cr3" :: "a" (new_cr3));
}

// Create a page directory. The page directory entries for the kernel's
// address space are duplicated.
// Caller is expected to provide storage for the 'struct pagedir'.
// The address of the 'struct pagedir' must not change!
// Returns 0=ok, 1=error (out of memory)
int pagedir_create(struct pagedir *pagedir);

// Destroy a page directory
void pagedir_destroy(struct pagedir *pagedir);

// Alter the current memory map: map a physical region to the
// current virtual address space.
// Flags are PTE_* constants
// Return: 0=ok, 1=error
int map_mem(intptr_t physical, intptr_t virtual, size_t n_pages, int flags);

// Sets up an initial 'struct pagedir' to represent the initial page directory
// (that was created in start.asm)
void paging_init(void);

// Switch to a page directory
void pagedir_switch(struct pagedir *pagedir);

#endif
