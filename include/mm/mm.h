#ifndef MM_MM_H
#define MM_MM_H

#include "stdint.h"

extern int _phys_base[]; // defined in linker script
extern int _virt_base[]; // defined in linker script

#define PAGE_SIZE 4096
#define KERNEL_PHYS_BASE ((intptr_t) _phys_base)
#define KERNEL_VIRT_BASE ((intptr_t) _virt_base)

// these are called during startup from the code which reads
// memory information (eg. from multiboot information)
void pmem_init_set_freemem_base(intptr_t freemem_base);
void pmem_init_mark_free(intptr_t region_start, intptr_t region_end);
size_t pmem_get_size(void); // return size of physical memory
void mem_init(void); // main initialization function

// Allocate and free physical pages

// Allocate between min_pages and max_pages continuous physical pages, returns
// the number of pages allocated. If you want to allocate an exact
// number of pages, use the same number for min_pages and max_pages.
size_t alloc_pages(size_t min_pages, size_t max_pages, intptr_t *start_addr);

// Free pages allocated with alloc_pages
void free_pages(intptr_t start_addr, size_t n_pages);

// Allocate and free kernel virtual pages
// These work the same way as alloc_pages and free_pages, only they allocate
// *virtual* pages in the kernel's address space. No actual memory is
// allocated.
size_t alloc_kvpages(size_t min_pages, size_t max_pages, intptr_t *start_addr);
void free_kvpages(intptr_t start_addr, size_t n_pages);

// Allocate and free kernel virtual pages and physical pages, and perform
// a mapping. Physical page addresses are put into an array at phys_addresses (which can be NULL)
size_t alloc_kvmem(size_t min_pages, size_t max_pages, void **start_addr, intptr_t *phys_addresses);
void free_kvmem(void *start_addr, size_t n_pages);

// Set up the page fault handler
void set_pagefault_handler(void);

#endif
