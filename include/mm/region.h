#ifndef MM_REGION_H
#define MM_REGION_H

#include "mm/mm.h"
#include "mm/paging.h"
#include "stdint.h"

#define P_R_SHARED 1         // shared mapping

// physical memory region
struct p_region {
    unsigned flags : 1;      // P_R_*
    unsigned start : 20;     // region start / PAGE_SIZE
    unsigned ref_count : 12; // number of virtual regions using this physical region
    unsigned len : 20;       // region length (number of pages)
};

#define VM_R_WRITABLE 1      // region is writable

// virtual address space region
struct vm_region {
    struct vm_region *prev, *next;
    unsigned flags : 1;  // VM_R_*
    unsigned start : 20; // region start / PAGE_SIZE
    unsigned len : 20;   // region length (number of pages)
    struct p_region *p_region;
};

// address space abstraction
struct addr_space {
    struct vm_region *regions; // doubly-linked-list of vm_region structs
                               // this list isn't sorted, but it a good candidate for
                               // optimization.
    struct pagedir pagedir;
};

// Create an empty/destroy an address space object
struct addr_space *addr_space_new(void);
void addr_space_delete(struct addr_space *aspace);

// Add a virtual region to an address space that is backed
// by a new physical region.
struct vm_region *vm_region_new_physical(struct addr_space *aspace,
  intptr_t virtual, size_t n_pages);

// Add a virtual region to an address space that is backed
// by a physical region with a pre-determined address.
struct vm_region *vm_region_new_physical_fixed(struct addr_space *aspace,
  intptr_t virtual, intptr_t physical, size_t n_pages);

// Add a virtual region to an address space that is backed
// by a new physical region, and is set for shared memory access.
struct vm_region *vm_region_new_physical_shared(struct addr_space *aspace,
  intptr_t virtual, size_t n_pages);

// Remove a virtual region from an address space, and release the associated
// physical region
void vm_region_remove(struct addr_space *aspace, struct vm_region *vm_r);

// Duplicate a region. This works for both shared and non-shared mappings.
// Non-shared mappings will be marked for copy-on-write.
struct vm_region *vm_region_dup(struct addr_space *new_addr_space, struct vm_region *vm_r, intptr_t virtual);

// Switch address spaces
void addr_space_switch(struct addr_space *new_addr_space);

#endif
