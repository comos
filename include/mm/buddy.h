#ifndef MM_BUDDY_H
#define MM_BUDDY_H

#include "stdint.h"

// architecture dependant!
typedef unsigned long bit_t;
#define BITS 32
#define LOG_BITS 5

struct buddy {
    size_t n_chunks; // number of leaf nodes
    int height; // tree height (number of bitmaps[] entries)
    bit_t *bitmaps;
};

void get_buddy_size(size_t n_chunks, int *height, size_t *struct_size);
void buddy_init(struct buddy *buddy, size_t n_chunks, int height, size_t struct_size);
unsigned int buddy_alloc(struct buddy *buddy, size_t min_size, size_t max_size, size_t *allocated_size);
void buddy_free(struct buddy *buddy, unsigned int o, size_t size);
void buddy_mark_used(struct buddy *buddy, unsigned int start, size_t count);

#endif

