#ifndef STDINT_H
#define STDINT_H

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long int uint64_t;

typedef unsigned short wchar_t;
typedef unsigned long size_t;
typedef unsigned long intptr_t;
typedef signed long ssize_t;
typedef signed long ptrdiff_t;

#endif

