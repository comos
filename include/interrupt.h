#ifndef INTERRUPT_H
#define INTERRUPT_H

#include "cpustruct.h"

// interrupt flags
enum {
    INTR_USER = 0x01,   // user can invoke swi (eg. int 0x80)
};

// This is the stack structure that arises
// from an interrupt
struct interrupt_stack {
    struct pusha_struct r; // caused by 'pusha' in isr
    uint32_t error_code, eip, cs, eflags, esp, ss; // pushed by cpu
};

// function pointer type for interrupt handlers
typedef void (* isr_t)(int vector, struct interrupt_stack *);

isr_t interrupt_handlers[256];

uint32_t default_handler_ptr;
uint32_t num_installed_interrupts;

// start the interrupt system
void interrupt_init(void);

// set up an interrupt handler for interrupt 'vector'
void interrupt_set_handler(int vector, isr_t handler);

// remove an interrupt handler
void interrupt_clear_handler(int vector);

// set interrupt flags
void interrupt_set_flags(int vector, int flags);

// get an interrupt vector from an IRQ
int irq_to_int(int irq);

// get the irq number behind an interrupt vector
int int_to_irq(int vector);

// unmask (enable) IRQ n
void unmask_irq(int n);

// mask (disable) IRQ n
void mask_irq(int n);

// acknowledge IRQ n
void ack_irq(int n);

#endif
