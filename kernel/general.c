#include "general.h"
#include "stdint.h"
#include "stdlib.h"
#include "console.h"

int strcmp(const char *str1, const char *str2)
{
    // the actual compare is done by memcmp()
    size_t len1, len2, smallest_len;
    int cmp;
    len1 = strlen(str1);
    len2 = strlen(str2);
    smallest_len = len1;
    if (len2 < smallest_len){
        smallest_len = len2;
    }
    // compare as many bytes as both strings have
    cmp = memcmp(str1, str2, smallest_len);
    if (cmp == 0){
        // the bits we compared were identical, but...
        if (len1 > len2){
            // str1 was longer than str2
            return -1;
        } else if (len1 < len2){
            // str2 was longer than str1
            return 1;
        } else {
            // yes, they were exactly the same
            return 0;
        }
    } else {
        // the bits we compared weren't the same
        // any extra bits after what we compared
        // won't make any difference
        return cmp;
    }
}

void panic_internal(const char *file, int line, const char *fmt, ...)
{
    cli();
    va_list ap;
    console_set_fcolour(get_vterm(0), COLOR_BRIGHT_RED); // set a noticable colour
    console_printf(get_vterm(0), "*** PANIC (%s:%d): ", file, line);
    // print the actual message
    va_start(ap, fmt);
    console_vprintf(get_vterm(0), fmt, ap);
    va_end(ap);
    // print a newline
    console_putchar(get_vterm(0), '\n');
    // completely halt
    hlt();
}

// Compare two strings. Should return -1 if
// str1 < str2, 0 if they are equal or 1 otherwise.
/*
int strcmp(char *str1, char *str2)
{
      int i = 0;
      int failed = 0;
      while(str1[i] != '\0' && str2[i] != '\0')
      {
          if(str1[i] != str2[i])
          {
              failed = 1;
              break;
          }
          i++;
      }
      // why did the loop exit?
      if( (str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0') )
          failed = 1;

      return failed;
}
*/

// Copy the NULL-terminated string src into dest, and
// return dest.
char *strcpy(char *dest, const char *src)
{
    memcpy(dest, src, strlen(src) + 1); // copy entire string + null byte
    return dest;
}

char *strncpy(char *dest, const char *src, size_t n)
{
    size_t len = strlen(src);
    if (len > n){
        len = n;
    }
    memcpy(dest, src, len);
    // pad the remainder of the string with null bytes!
    memset(dest + len, 0, n - len);
    return dest;
}

// Concatenate the NULL-terminated string src onto
// the end of dest, and return dest.
/*
char *strcat(char *dest, const char *src)
{
    while (*dest != 0)
    {
        *dest = *dest++;
    }

    do
    {
        *dest++ = *src++;
    }
    while (*src != 0);
    return dest;
}
*/
/*
int strlen(char *src)
{
    int i = 0;
    while (*src++)
        i++;
    return i;
}
*/
char * strtok( char * s1, const char * s2 )
{
    static char * tmp = NULL;
    const char * p = s2;

    if ( s1 != NULL )
    {
        /* new string */
        tmp = s1;
    }
    else
    {
        /* old string continued */
        if ( tmp == NULL )
        {
            /* No old string, no new string, nothing to do */
            return NULL;
        }
        s1 = tmp;
    }

    /* skipping leading s2 characters */
    while ( *p && *s1 )
    {
        if ( *s1 == *p )
        {
            /* found seperator; skip and start over */
            ++s1;
            p = s2;
            continue;
        }
        ++p;
    }

    if ( ! *s1 )
    {
        /* no more to parse */
        return ( tmp = NULL );
    }

    /* skipping non-s2 characters */
    tmp = s1;
    while ( *tmp )
    {
        p = s2;
        while ( *p )
        {
            if ( *tmp == *p++ )
            {
                /* found seperator; overwrite with '\0', position tmp, return */
                *tmp++ = '\0';
                return s1;
            }
        }
        ++tmp;
    }

    /* parsed to end of string */
    return ( tmp = NULL );
}
