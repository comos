#include "stdlib.h"
#include "console.h"
#include "mp_tables.h"

static struct mp_processor mp_processor[8];
static struct mp_bus mp_bus[8];
static struct mp_io_apic mp_io_apic[4];
static struct mp_io_interrupt mp_io_interrupt[4*24]; // # of IO APICs * MAX # of IO Redirections per IO APIC
static struct mp_local_interrupt mp_local_interrupt[32];
static struct mp_counts mp;

static void parse_mp_config_tables(uint32_t offset)
{
    uint16_t entry_count = 0;
    uint32_t address_of_local_apic = 0;
    uint32_t processor_entries = 0, bus_entries = 0;
    uint32_t io_apic_entries = 0, io_interrupt_entries = 0;
    uint32_t local_interrupt_entries = 0;
        
    memcpy(&entry_count, (void *)(offset + 0x22), sizeof(uint16_t));
    memcpy(&address_of_local_apic, (void *)(offset + 0x24), sizeof(uint16_t));
    offset += 0x2C; /* jump over the header */

    while(entry_count--)
    {
        switch(*(uint8_t*)offset)
        {
            case 0x00:
                trace("MP Processor Entry Found:\n");
                
                if(processor_entries >= 8)
                {trace(" - MP Too Many Processor Entries\n"); offset += 0x14; break;}
                
                memcpy(&mp_processor[processor_entries], (void *)(offset), 0x14);
                
                if((mp_processor[processor_entries].cpu_flags & 0x01) == 0)
                {trace(" - MP Unusable Processor Found \n");}
                
                processor_entries++;
                offset += 0x14;
                break;
                    
            case 0x01:
                if(bus_entries >= 8)
                        {trace(" - MP Too Many BUS Entries\n"); offset += 0x08; break;}
                        
                        memset(&mp_bus[bus_entries].bus_type_string, 0, 7);
                memcpy(&mp_bus[bus_entries], (void *)(offset), 0x08);
                        
                trace("MP BUS Found:\n");
                        trace("    BUS(%u): ID: %u | Type: %s \n", bus_entries, 
                                                    mp_bus[bus_entries].bus_id,
                                           mp_bus[bus_entries].bus_type_string);

                offset += 0x08;
                bus_entries++;
                break;  

            case 0x02:
                if(io_apic_entries >= 8)
                {trace(" - MP Too Many IO APIC Entries\n"); offset += 0x08; break;}
                
                memcpy(&mp_io_apic[io_apic_entries], (void *)(offset), 0x08);
                
                trace("MP IO APIC Found:\n");
                trace("    IO APIC(%u): ID: %u | Address: 0x%X \n", io_apic_entries,
                                                     mp_io_apic[io_apic_entries].id,
                                                mp_io_apic[io_apic_entries].address);
                offset += 0x08;
                io_apic_entries++;    
                break;

            case 0x03:
                memcpy(&mp_io_interrupt[io_interrupt_entries], (void *)(offset), 0x08);
                /* un-comment these only if you want a screen-full of io interrupt entries */
                /*trace("MP IO Interrupt Found:\n");
                trace("    IO Interrupt(%u): Type: %u | SRC_ID: %x | DST_ID: %x \n", 
                                                               io_interrupt_entries,
                                     mp_io_interrupt[io_interrupt_entries].int_type,                     
                                       mp_io_interrupt[io_interrupt_entries].src_id,
                                       mp_io_interrupt[io_interrupt_entries].dst_id);*/
                offset += 0x08;
                io_interrupt_entries++;
                break;

            case 0x04:
                memcpy(&mp_local_interrupt[local_interrupt_entries], (void *)(offset), 0x08);
                        /* un-comment these only if you want a screen-full of io interrupt entries */
                        /*trace("MP Local Interrupt Found:\n");
                        trace("    Local Interrupt(%u): Type: %u | SRC_ID: %x | DST_ID: %x \n", 
                                                                       local_interrupt_entries,
                                          mp_local_interrupt[local_interrupt_entries].int_type,                     
                                             mp_local_interrupt[local_interrupt_entries].src_id,
                                             mp_local_interrupt[local_interrupt_entries].dst_id);*/
                offset += 0x08;
                local_interrupt_entries++;
                break;
                    
            default:
                trace(" - MP Unknown CONFIG Entry Found (%u) \n", *(uint8_t*)offset);
                entry_count = 0;
                break;
        }
    }
    
    mp.processor_count = processor_entries;
    mp.bus_count = bus_entries;
    mp.io_apic_count = io_apic_entries;
    mp.io_interrupt_count = io_interrupt_entries;
    mp.local_interrupt_count = local_interrupt_entries;
    
    console_printf(get_vterm(0), "Total processor count: %u \n", mp.processor_count);
    console_printf(get_vterm(0), "Total bus count: %u \n", mp.bus_count);
    console_printf(get_vterm(0), "Total IO APIC count: %u \n", mp.io_apic_count);
    console_printf(get_vterm(0), "Total IO interrupt count: %u \n", mp.io_interrupt_count);
    console_printf(get_vterm(0), "Total local interrupt count: %u \n", mp.local_interrupt_count);
}

static void parse_mp_fps(uint32_t offset)
{
    uint32_t mp_config_address = *(uint32_t*)(offset + 4);
    trace("MP CONFIG TABLES found @ 0x%X \n", mp_config_address);
    
    parse_mp_config_tables(mp_config_address);
}

void parse_mp_tables(void)
{
    uint32_t i = 0; /* we really don't want this to get optimized away */
    const char * sig = "_MP_";    /* this a 'char' array, not an uint8_t array */
    
    mp.processor_count = 0; /* just incase something goes wrong, we can just assume there is 1 cpu */
    
    for(i = 0x9FC00; i < 0xA0000; i += 16)  /* scan area 1 */
    {
          if(!memcmp((void *)i, sig, 4))
          {
              trace("MP FPS found @ 0x%X \n", i); 
              parse_mp_fps(i);
              return;
          }
    }
    
    for(i = 0xF0000; i < 0x100000; i += 16) /* scan area 2 */
    {
          if(!memcmp((void *)i, sig, 4))
          {
              trace("MP FPS found @ 0x%X \n", i); 
              parse_mp_fps(i);
              return;
          }
    }

}
