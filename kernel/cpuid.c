#include "cpuid.h"
#include "stddef.h"
#include "console.h"

struct cpuid_s cpuid;

/* CPUID(eax == 1) */
void cpuid_get_features(void)
{
    uint32_t eax, ebx, ecx, edx;
    
    eax = 0x01;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));
    
    cpuid.features_edx = edx;
    cpuid.features_ecx = ecx;
    
    cpuid.stepping =    (eax & 0x0F);
    cpuid.model =       ((eax >> 4) & 0x0F);
    cpuid.family =      ((eax >> 8) & 0x0F);
    cpuid.type =        ((eax >> 12) & 0x03);
    
    cpuid.cache_line_size =     ((ebx >> 8) & 0xFF) * 8; /* cache_line_size * 8 = size in bytes */
    cpuid.logical_processors =  ((ebx >> 16) & 0xFF);    /* # logical cpu's per physical cpu */
    cpuid.lapic_id =            ((ebx >> 24) & 0xFF);    /* Local APIC ID */
    
    console_printf(get_vterm(0), "    Family: 0x%X | Model: 0x%X | Stepping: 0x%X | Type: 0x%X \n",  
                                                                       cpuid.family, 
                                                                       cpuid.model, 
                                                                       cpuid.stepping, 
                                                                       cpuid.type);
    console_printf(get_vterm(0), "    Cache Line Size: %u bytes | Local APIC ID: 0x%X \n", 
                                                                       cpuid.cache_line_size, 
                                                                       cpuid.lapic_id);
} 

void cpuid_get_cpu_brand(void)
{
    uint32_t eax, ebx, ecx, edx;
    
    eax = 0x80000002;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));
    
    cpuid.cpu_brand[48] = 0;     /* init cpu_brand to null-terminate the string */
    *(uint32_t*)(cpuid.cpu_brand + 0 ) = eax;
    *(uint32_t*)(cpuid.cpu_brand + 4 ) = ebx;
    *(uint32_t*)(cpuid.cpu_brand + 8 ) = ecx;
    *(uint32_t*)(cpuid.cpu_brand + 12) = edx;
    
    eax = 0x80000003;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));

    *(uint32_t*)(cpuid.cpu_brand + 16) = eax;
    *(uint32_t*)(cpuid.cpu_brand + 20) = ebx;
    *(uint32_t*)(cpuid.cpu_brand + 24) = ecx;
    *(uint32_t*)(cpuid.cpu_brand + 28) = edx;
    
    eax = 0x80000004;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));

    *(uint32_t*)(cpuid.cpu_brand + 32) = eax;
    *(uint32_t*)(cpuid.cpu_brand + 36) = ebx;
    *(uint32_t*)(cpuid.cpu_brand + 40) = ecx;
    *(uint32_t*)(cpuid.cpu_brand + 44) = edx;
    
    console_printf(get_vterm(0), "    CPU Brand: %s \n", cpuid.cpu_brand);
}

void parse_cpuid(void)
{
    uint32_t eax, ebx, ecx, edx;
    
    eax = 0x00;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));
    
    cpuid.max_basic_input_val = eax;
    memset(cpuid.manufacturer_string, 0, 13);
    *(uint32_t*)(cpuid.manufacturer_string + 0) = ebx;
    *(uint32_t*)(cpuid.manufacturer_string + 4) = edx;
    *(uint32_t*)(cpuid.manufacturer_string + 8) = ecx;
    
    console_printf(get_vterm(0), "CPUID: \n");
    console_printf(get_vterm(0), "    Manufacturer String: %s \n", cpuid.manufacturer_string);
    
    if(cpuid.max_basic_input_val >= 1){
        cpuid_get_features();
    }
    
    eax = 0x80000000;
    asm volatile ("cpuid" : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) : "a"(eax));
    
    cpuid.max_ext_input_val = eax; 
    
    if(cpuid.max_ext_input_val >= 0x80000004){
        cpuid_get_cpu_brand();
    }
}
