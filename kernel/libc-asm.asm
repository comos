bits 32

; assembly-language implementations of some of the most common
; standard C library functions

global memcpy, memmove

; a valid memmove implementation is actually
; a valid memcpy implementation, so we can
; use the same implementation for both

memmove:
memcpy:
    ; (void *dest, void *src, size_t count)
    push  ebp
    mov   ebp,esp
    push  esi
    push  edi
    mov   eax,[ebp+8]  ; dest
    mov   ecx,[ebp+12] ; src
    mov   edi,eax
    mov   esi,ecx
    cmp   ecx,eax
    jb    .backwards
    ; copy forwards - lower addresses to higher addresses
    cld
    mov   ecx,[ebp+16] ; count
    mov   edx,ecx
    shr   ecx,2        ; count div 4
    rep   movsd        ; copy 4 bytes at a time
    mov   ecx,edx
    and   ecx,3        ; count mod 4
    rep   movsb        ; copy the remaining bytes
    jmp   .finish
.backwards:
    ; copy backwards - higher addresses to lower addresses
    std
    mov   ecx,[ebp+16] ; count
    mov   edx,ecx
    add   esi,ecx      ; set esi/edi to point at byte *after* last byte
    add   edi,ecx
    shr   ecx,2        ; count div 4
    test  ecx,ecx
    jz    .bytesonly
    sub   esi,4        ; point at last word
    sub   edi,4
    rep   movsd        ; copy 4 bytes at a time
    add   esi,4
    add   edi,4
.bytesonly:
    mov   ecx,edx
    and   ecx,3        ; count mod 4
    dec   esi          ; point at last byte
    dec   edi
    rep   movsb        ; copy the remaining bytes
    cld                ; leave the DF (direction flag) how you would like to find it
.finish:
    mov   eax,[ebp+8]  ; return dest
    pop   edi          ; restore esi/edi
    pop   esi
    mov   esp,ebp
    pop   ebp
    ret

global memset
memset:
    ; (void *dest, int val, size_t count)
    push  ebp
    mov   ebp,esp
    push  edi
    mov   eax,[ebp+12] ; val
    mov   ecx,0x01010101
    mul   ecx          ; copy val to each byte in eax
    mov   edi,[ebp+8]  ; dest
    mov   ecx,[ebp+16] ; count
    mov   edx,ecx
    shr   ecx,2        ; count div 4
    rep   stosd        ; store a word at a time
    mov   ecx,edx
    and   ecx,3        ; count mod 4
    rep   stosb        ; store a byte at a time
    mov   eax,[ebp+8]  ; return dest
    pop   edi
    mov   esp,ebp
    pop   ebp
    ret

global wmemset
wmemset:
    push  ebp
    mov   ebp,esp
    push  edi
    mov   eax,[ebp+12] ; val
    mov   ecx,0x00010001
    mul   ecx ; copy val to each halfword in eax
    mov   edi,[ebp+8]  ; dest
    mov   ecx,[ebp+16] ; count
    mov   edx,ecx
    shr   ecx,1        ; count div 2 (count is number of halfwords, not bytes)
    rep   stosd        ; store a word at a time
    mov   ecx,edx
    test  ecx,1
    jz    .noextra
    ; there's one more halfword to store
    stosw ; store it
.noextra:
    mov   eax,[ebp+8]  ; return dest
    pop   edi
    mov   esp,ebp
    pop   ebp
    ret

global strlen
strlen:
    ; use a simple byte-at-a-time algorithm!
    ; this is slow but simple.
    push  ebp
    mov   ebp,esp
    push  esi
    mov   esi,[ebp+8]  ; str
    cld
.loop:
    lodsb
    test  al,al
    jnz   .loop
    mov   eax,esi
    dec   eax
    sub   eax,[ebp+8]  ; subtract original str pointer
    pop   esi
    mov   ebp,esp
    pop   ebp
    ret

global memcmp
memcmp:
    ; (const void *src1, const void *src2, size_t count)
    push  ebp
    mov   ebp,esp
    push  ebx
    mov   eax,[ebp+8]  ; src1
    mov   edx,[ebp+12] ; src2
    mov   ecx,[ebp+16] ; count
    ; do it the simple but slow way - byte at a time!
.loop:
    mov   bl,[eax]
    mov   bh,[edx]
    cmp   bh,bl
    jb    .lower
    ja    .higher
    ; the two bytes are the same - carry on
    inc   eax
    inc   edx
    dec   ecx
    jnz   .loop
    xor   eax,eax      ; reached end - return zero
    jmp   .finish
.lower:
    mov   eax,-1
    jmp   .finish
.higher:
    mov   eax,1
.finish:
    pop   ebx
    mov   esp,ebp
    pop   ebp
    ret

