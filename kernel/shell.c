#include "general.h"
#include "shell.h"
#include "console.h"
#include "keyboard.h"
#include "critical.h"
#include "stdlib.h"
#include "task.h"
#include "cpuid.h"
#include "interrupt.h"

/* the character buffer is a ring buffer */
#define key_buf_size    256
uint8_t  		        key_buffer[key_buf_size];      /* character buffer */
uint8_t					next_key;
fnList			        fnFunctions[255] = {0};
char    			    fnCommands[255][50];
char                    fnDescriptions[255][50];
volatile uint8_t		            fnPos = 0;

uint8_t show_cmd_pos = 0;
static fnList show_cmd_functions[256] = {0};
static char show_cmds[256][32];

typedef struct lastCom
{
    char   buffer[256];
    uint8_t  length;
    struct lastCom *prevCom;
    struct lastCom *nextCom;
} lastCom_t;

lastCom_t *root_com;
lastCom_t *tail_com;
lastCom_t *cur_com;
   
/* shell_keypress(key) takes input directly from the keyboard 
   handler, places it in the buffer, and decrements the 
   buffer pointer */
uint32_t key_pos_head = 0;
void shell_keypress(uint8_t key)
{
    key_buffer[(key_pos_head % key_buf_size)] = key;
    key_pos_head += 1;
}

/* shell_get_next_key() finds the top of the character buffer 
   and returns the character.  If the buffer is empty, return 0. */
uint32_t key_pos_tail = 0;
uint8_t shell_get_next_key()
{   
    if((key_pos_tail % key_buf_size) == (key_pos_head % key_buf_size))
    {return 0;}

    next_key = key_buffer[(key_pos_tail % key_buf_size)];
    if(!next_key){return 0;}
    
    key_pos_tail += 1;
    return next_key;
}

/* shell_task() starts by telling the keyboard handler to 
   send the ascii key to shell_keypress(key).  This places 
   the key into our ring buffer.  Then this task function 
   searches the buffer using a head-tail method and prints 
   the characters to the screen. */
void kshell_task()
{
    critical_enter();
	uint8_t		lkey = 0;
	uint8_t		temp = 0;
	uint8_t		keyBPos = 0;
	uint8_t		commandBuffer[key_buf_size-1] = {0};

    key_pos_head = key_pos_tail = 0;
	keyboard_register_listener(&shell_keypress);
	//Register useable shell functions
	kshell_function("ver",&shell_ver,"Displays the kernel version.");
	kshell_function("cmds",&shell_cmds,"Lists the available commands.");
	kshell_function("clear",&shell_clear,"Clears the screen.");
	kshell_function("ps",&shell_ps,"Lists the currently running tasks.");
	kshell_function("show",&shell_show,"Lists object info. (usage: show <object>)");
	
	kshell_add_show_child("version", &shell_ver);
	kshell_add_show_child("commands", &shell_cmds);
	kshell_add_show_child("processes", &shell_ps);
	kshell_add_show_child("tasks", &shell_ps);
	kshell_add_show_child("cpuid", &shell_cpuid);
	kshell_add_show_child("system-info", &shell_system_info);
	kshell_add_show_child("interrupts", &shell_interrupts);
	///////////////////////////////////
    critical_exit();    
    
	for(;;)
	{
        while(1)
		{
		    critical_enter();
			console_printf(get_vterm(0), "\n$%s>", "/home/");
			critical_exit();
			
			while(lkey != '\n')
			{
				lkey = shell_get_next_key();
				
				if(lkey == '\b')
				{
					if(keyBPos > 0)
					{
					    critical_enter();
						console_printf(get_vterm(0), "\b \b");
						critical_exit();
						keyBPos--;
					}
				}
				else if(lkey == '\t')
				{
                    console_printf(get_vterm(0), "    ");
				}
				else if(lkey == K_VTERM_1 || 
				        lkey == K_VTERM_2 ||
				        lkey == K_VTERM_3 ||
				        lkey == K_VTERM_4)
				{
				    critical_enter();
				    if(keyBPos){keyBPos--;}
				    console_printf(get_vterm(0), "\nswitching to vterm(%u)",lkey-K_VTERM_1);
				    console_printf(get_vterm(0), "\n$%s>", "/home/");
                switch_vterm(get_vterm(lkey - K_VTERM_1));
                critical_exit();
            }
            else if(lkey == K_UP /* up */)
            {
                while(keyBPos>0)
                {   //This can be dramaticly optimized...like..one line of code...
                    critical_enter();
                    console_printf(get_vterm(0), "\b \b");
                    critical_exit();
                    keyBPos--;
                }
                if(cur_com != 0)
                {
                    for(temp = 0; temp < cur_com->length; temp++)
                    {
                        critical_enter();
                        console_printf(get_vterm(0), "%c",cur_com->buffer[temp]);
                        critical_exit();
                        commandBuffer[keyBPos] = cur_com->buffer[temp];
                        if(keyBPos < 254){ keyBPos++; }
                        else{ break; }
                    }
                    if(cur_com->prevCom != 0){ cur_com = cur_com->prevCom; }
                }
            }
            
            else if(lkey == K_DOWN /* down */)
            {
                while(keyBPos > 0)
                {   //This can be dramaticly optimized...like..one line of code...
                    critical_enter();
                    console_printf(get_vterm(0), "\b \b");
                    critical_exit();
                    keyBPos--;
                }
                
                if(cur_com != 0)
                {
                    for(temp = 0; temp < cur_com->length; temp++)
                    {
                        critical_enter();
                        console_printf(get_vterm(0), "%c",cur_com->buffer[temp]);
                        critical_exit();
                        commandBuffer[keyBPos] = cur_com->buffer[temp];
                        if(keyBPos < 254){ keyBPos++; }
                        else{ break; }
                    }
                    if(cur_com->nextCom != 0) cur_com = cur_com->nextCom;
                }
                
            }
            
            else if(lkey > 0)
            {
                if(lkey!='\n')
                {
                    critical_enter();
                    console_printf(get_vterm(0), "%c",lkey);
                    commandBuffer[keyBPos] = lkey;
                    if(keyBPos < 254){ keyBPos++; }
                    else{ break; }
                    critical_exit();
                }
            }
        }
        
        critical_enter();
        console_printf(get_vterm(0), "1\n");
        commandBuffer[keyBPos] = '\0';
        console_printf(get_vterm(0), "2\n");
        cur_com = tail_com;
        
        cur_com->nextCom = (lastCom_t*)malloc(sizeof(lastCom_t));
        console_printf(get_vterm(0), "3\n");
        cur_com->nextCom->prevCom = cur_com;
        cur_com->nextCom->nextCom = 0;
        cur_com->nextCom->length = keyBPos;
        console_printf(get_vterm(0), "4\n");
        strcpy((char *)cur_com->nextCom->buffer,(char *)commandBuffer);
        console_printf(get_vterm(0), "5\n");
        cur_com = cur_com->nextCom;
        tail_com = cur_com;
        cur_com = tail_com;
        console_printf(get_vterm(0), "6\n");
        critical_exit();
        kshell_lookup((char*)commandBuffer);
        lkey = 0;
        keyBPos = 0;
    }
}
}

uint8_t tmp[key_buf_size];

void kshell_lookup(char buffer[key_buf_size-1])
{
uint8_t x = 0;

critical_enter();

while(buffer[x]!='\n' && buffer[x]!=' ' && buffer[x]!='\0' && x < key_buf_size-1)
{
    tmp[x] = buffer[x];
    x++;
}

console_printf(get_vterm(0), "7\n");

if(x < key_buf_size-1)
{
    tmp[x] = 0;
    for(x = 0; x < fnPos; x++)
    {
        console_printf(get_vterm(0), ":%u, ", x);
        console_printf(get_vterm(0), "\"%s\", ", (char *)tmp); 
        console_printf(get_vterm(0), "\"%s\", ", (char *)fnCommands[x]); 
        console_printf(get_vterm(0), "0x%X, ", (uint32_t)tmp);
        console_printf(get_vterm(0), "%u\n", fnPos); 

        if(!strcmp((char *)tmp, fnCommands[x]))
        {
            console_printf(get_vterm(0), "8\n");
            console_printf(get_vterm(0), "fnFunctions[%u](0x%X)\n", x, (uint32_t)buffer);
            critical_exit();
            fnFunctions[x](buffer);
            return;
        }
    }
    buffer[x] = 0;
}

buffer[key_buf_size-2] = 0; // just in case
console_printf(get_vterm(0), "\nCommand %s not found.", buffer);
}


void kshell_function(char fname[50], fnList fFunction, char *fdesc)
{
critical_enter();
fnFunctions[fnPos] = fFunction;
memset(fnCommands[fnPos], 0, 50);
memcpy(fnCommands[fnPos], fname, 50);
memset(fnDescriptions[fnPos], 0, 50);
strcpy(fnDescriptions[fnPos++], fdesc);
critical_exit();
}

void kshell_add_show_child(char *fname, fnList fFunction)
{
show_cmd_functions[show_cmd_pos] = fFunction;
memset(show_cmds[show_cmd_pos], 0, 32);
strcpy(show_cmds[show_cmd_pos++], fname);
}

/***************************************
Place all shell functions below here.
***************************************/
void shell_ver(char * buffer)
{
critical_enter();
console_printf(get_vterm(0), "\nCommunity OS version %s i386 (codenamed: %s)", kernel_version, kernel_codename);
critical_exit();
}

void shell_cmds(char * buffer)
{
int x;
critical_enter();
for(x=0;x<fnPos;x++)
{
    console_printf(get_vterm(0), "\nindex = {%i}, command = {%s}, desc = {%s}", x,fnCommands[x],fnDescriptions[x]);
}
critical_exit();
}

void shell_clear(char * buffer)
{
console_clear(curcons);
}

void shell_ps(char * buffer)
{
uint32_t i, tasks_found = 0;

critical_enter();

console_printf(get_vterm(0), "\nactive tasks: %u, sleeping tasks: %u, maximum tasks allowed: %u \n", num_active_tasks,
                                                                                       num_sleeping_tasks,
                                                                                       MAX_TASKS);
for(i = 0; i < MAX_TASKS; i++)
{
    if(task[i].state == TASK_READY)
    {
        tasks_found++;
        console_printf(get_vterm(0), "\ntask(%u): priority: %u, name: \"%s\"", i, task[i].priority, task[i].name);
        
        if(tasks_found >= num_active_tasks){
            break;
        }
    }
}
critical_exit();
}

void shell_cpuid(char * buffer)
{
critical_enter();
console_printf(get_vterm(0), "\n ******************************************************* ");
console_printf(get_vterm(0), "\n ***  CPUID Information:                             *** ");
console_printf(get_vterm(0), "\n **   - This function prints the information parsed   ** ");
console_printf(get_vterm(0), "\n *      from all available CPUID instructions.         * ");
console_printf(get_vterm(0), "\n *    - If information is missing, it has been         * ");
console_printf(get_vterm(0), "\n **     detected as unavailable or erroneous.         ** ");
console_printf(get_vterm(0), "\n ***  - Written by: 01000101                         *** ");
console_printf(get_vterm(0), "\n ******************************************************* \n");

console_printf(get_vterm(0), "\nmax normal EAX input: 0x%X\nmax extended EAX input: 0x%X", cpuid.max_basic_input_val,
                                                                             cpuid.max_ext_input_val);
                                                                             
if(cpuid.max_basic_input_val >= 1){
console_printf(get_vterm(0), "\ncpuid(1).ecx: 0x%X\ncpuid(1).edx: 0x%X", cpuid.features_ecx, cpuid.features_edx);
console_printf(get_vterm(0), "\nstepping: 0x%X, model: 0x%X, family: 0x%X, type: 0x%X", cpuid.stepping, 
                                                                          cpuid.model, 
                                                                          cpuid.family,
                                                                          cpuid.type);
console_printf(get_vterm(0), "\ncache-line size: %u bytes", cpuid.cache_line_size);
}
                                                                                
console_printf(get_vterm(0), "\nmanufacturer: \"%s\"", cpuid.manufacturer_string);

if(cpuid.max_ext_input_val >= 0x80000004){
console_printf(get_vterm(0), "\ncpu brand: \"%s\"", cpuid.cpu_brand);
}

console_printf(get_vterm(0), "\n");

critical_exit();
}

void shell_interrupts(char * buffer)
{   
critical_enter();

console_printf(get_vterm(0), "\ninstalled interrupt service routines: %u\n", num_installed_interrupts);
console_printf(get_vterm(0), "\nexceptions are mapped to irqs 0-31, irqs 32-255 are user defined");
console_printf(get_vterm(0), "\ninterrupt controller: PIC"); // will be replaced with IOAPIC/PIC detection
uint32_t i = 0;
for(i = 0; i < 256; i++)
{
    if((uint32_t)interrupt_handlers[i] != default_handler_ptr)
    {
        console_printf(get_vterm(0), "\nirq(%u) routes to address 0x%X", i, interrupt_handlers[i]);
    }
}
critical_exit();
}

void shell_system_info(char * buffer)
{
critical_enter();
console_printf(get_vterm(0), "\nfunction not yet implemented");
critical_exit();
}

void shell_show(char * buffer)
{
uint8_t i = 0;

for(i = 0; i < show_cmd_pos; i++)
{   
    if(!strcmp(buffer + 5, show_cmds[i])){ show_cmd_functions[i](buffer); return; }
}

critical_enter();
console_printf(get_vterm(0), "\nshow usage: show <object>");
    critical_exit();
}












