#include "console.h"
#include "stddef.h"
#include "stdint.h"
#include "stdlib.h"
#include "portio.h"
#include "serial.h"
#include "critical.h"
#include "chardev.h"

// Character device interface to consoles

static ssize_t console_write(struct chardev *, const void *, size_t);
static ssize_t console_read(struct chardev *, void *, size_t);

static struct chardev_functab console_functab = {
    console_read,
    console_write,
};

static ssize_t console_read(struct chardev *cdev, void *buf, size_t len)
{
    // not implemented
    return 0;
}

static ssize_t console_write(struct chardev *cdev, const void *buf, size_t len)
{
    struct vterm *vt = (struct vterm *) cdev;
    const char *c_buf = buf;
    ssize_t n_written = 0;
    while (len){
        console_putchar(vt, *c_buf++), --len, ++n_written;
    }
    return n_written;
}

uint16_t *video_ram = (uint16_t *) 0xB8000;
enum {
    lines = 25,
    columns = 80,
    max_vterms = 4,
};

// structure representing a virtual terminal
struct vterm {
    struct chardev cdev;
    uint16_t buf[lines * columns];   // buffer that stores the content of the virtual terminal when it is not active
    int x, y;                        // current cursor position
    int attrib;                      // current attribute (fg/bg colour)
    struct serial *serial;           // serial port, if set
};

static struct vterm *current_vterm = NULL;
static struct vterm vterms[max_vterms];

// scroll the console, if needed
static void scroll(struct vterm *vt)
{
    uint16_t *buf;
    if (vt->y >= lines){
        // shift the contents of the buffer up one line
        buf = vt == current_vterm ? video_ram : vt->buf;
        memmove(buf,        // start of the buffer
          buf + columns,    // start of the second line
          (lines - 1)* columns * sizeof *buf); // copy all but one line
        // clear the bottom line
        wmemset(buf + (lines - 1) * columns,
          (vt->attrib << 8) | ' ', columns);
        --vt->y;
    }
}

// get the hardware cursor position (the blinky line)
// used only during bootup, to see where the bootloader left it
static void get_hardware_cursor(struct vterm *vt)
{
    unsigned int tmp;
    outb(0x3D4, 14);
    tmp = inb(0x3D5) << 8;
    outb(0x3D4, 15);
    tmp |= inb(0x3D5);
    vt->y = tmp / columns;
    vt->x = tmp % columns;
}

// set the hardware cursor position, to please the user
static void set_hardware_cursor(struct vterm *vt)
{
    unsigned int tmp;
    tmp = vt->y * columns + vt->x;
    outb(0x3D4, 14);
    outb(0x3D5, tmp >> 8);
    outb(0x3D4, 15);
    outb(0x3D5, tmp);
}

void console_init(void)
{
    int i;
    
    for (i=0; i<max_vterms; ++i){
        vterms[i].cdev.functab = &console_functab;
        vterms[i].cdev.major = 4;
        vterms[i].cdev.minor = i + 1;
        vterms[i].serial = NULL;
        chardev_register(&vterms[i].cdev);
    }

    // clear only vt1..
    for (i=1; i<max_vterms; ++i){
        console_clear(vterms + i);
    }

    current_vterm = vterms;
    get_hardware_cursor(current_vterm);
}

void console_change_vram(uint16_t *new_video_ram)
{
    video_ram = new_video_ram;
}

struct vterm *get_vterm(int n)
{
    if (n >= 0 && n < max_vterms){
        return vterms + n;
    } else {
        return NULL;
    }
}

void switch_vterm(struct vterm *new_vterm)
{
    if (new_vterm != current_vterm){
        // store vram into buffer of old vt
        memcpy(current_vterm->buf, video_ram, lines * columns * sizeof *video_ram);
        // load vram from buffer of new vt
        memcpy(video_ram, new_vterm->buf, lines * columns * sizeof *video_ram);
        current_vterm = new_vterm;
        // set hardware cursor position
        set_hardware_cursor(new_vterm);
    }
}

void console_clear(struct vterm *vt)
{
    console_reset_colour(vt);
    wmemset((vt == current_vterm) ? video_ram : vt->buf,
      (vt->attrib << 8) | ' ', lines * columns);
    vt->x = vt->y = 0;
}

void console_putchar(struct vterm *vt, char c)
{
    if (vt->serial){
        serial_putchar(vt->serial, c);
    }

    // Handle a backspace, by moving the cursor back one space
    if (c == '\b' && vt->x > 0){
        --vt->x;
        // clear the character
        (vt == current_vterm ? video_ram : vt->buf)
          [vt->y * columns + vt->x] = ' ' | (vt->attrib << 8);
    } else if (c == '\t'){
        // tab character - align column number to a multiple of 8
        vt->x = (vt->x + 8) & ~(8 - 1);
    } else if (c == '\r'){
        // carriage return
        vt->x = 0;
    } else if (c == '\n'){
        // line feed
        vt->x = 0;
        ++vt->y;
    } else if (c >= ' '){
        // write the printing character to vram or the buffer
        (vt == current_vterm ? video_ram : vt->buf)
          [vt->y * columns + vt->x] = c | (vt->attrib << 8);
        ++vt->x;
    }
    if (vt->x >= columns){
        // line wrap
        vt->x = 0;
        ++vt->y;
    }
    scroll(vt);     // scroll, if needed
    if (vt == current_vterm){
        set_hardware_cursor(vt);
    }
}

void console_puts(struct vterm *vt, const char *str)
{
    while (*str){
        console_putchar(vt, *str++);
    }
}

void console_reset_colour(struct vterm *vt)
{
    console_set_fbcolour(vt, COLOUR_WHITE, COLOUR_BLACK); // easy
}

void console_set_fcolour(struct vterm *vt, int new_fcolour)
{
    vt->attrib = (vt->attrib & 0xF0) | new_fcolour;
}

void console_set_fbcolour(struct vterm *vt, int new_fcolour, int new_bcolour)
{
    vt->attrib = new_fcolour | (new_bcolour << 4);
}

void console_printf(struct vterm *vt, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    console_vprintf(vt, fmt, ap);
    va_end(ap);
}

void console_vprintf(struct vterm *vt, const char *fmt, va_list ap)
{
    // 256 characters ought to be enough for anybody!
    static char buf[256];
    vsnprintf(buf, 256, fmt, ap);
    buf[255] = '\0';
    console_puts(vt, buf);
}

void console_serial_enable(struct vterm *vt, struct serial *s)
{
    vt->serial = s;
}
