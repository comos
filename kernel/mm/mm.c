#include "mm/mm.h"
#include "mm/buddy.h"
#include "mm/paging.h"
#include "console.h"
#include "align.h"

// Boundaries of contiguous free memory.
// This is far from ideal.
static intptr_t freemem_low_bound;
static intptr_t freemem_high_bound;

// Physical buddy allocator
static struct buddy *physical_buddy = NULL;

// Virtual buddy allocator (for kernel virtual address space)
static struct buddy *virtual_buddy = NULL;

// Base of physical memory that we're allocating
static intptr_t phys_base;

// Base of virtual kernel memory that we're managing
static intptr_t virt_base;

void pmem_init_set_freemem_base(intptr_t freemem_base)
{
    freemem_low_bound = alignup(freemem_base, PAGE_SIZE);
}

void pmem_init_mark_free(intptr_t region_start, intptr_t region_end)
{
    // Don't use non-contiguous parts of memory. This might throw away
    // large chunks of physical memory that we'll never use!
    // It's easier this way for now. At least print a warning if any
    // gets wasted.
    if (region_start <= freemem_low_bound){
        freemem_high_bound = region_end;
    } else {
        trace("Throwing away %d MiB of memory!\n",
          uldivru(region_end - region_start, 1024 * 1024));
    }
}

size_t pmem_get_size(void)
{
    return freemem_high_bound - freemem_low_bound;
}

void mem_init(void)
{
    size_t n_chunks;
    int height;
    size_t struct_size, old_struct_size;
    intptr_t new_freemem_low_bound;

    // create the virtual buddy allocator (for kernel address space)

    n_chunks = (((intptr_t) PAGE_TAB) - KERNEL_VIRT_BASE) / PAGE_SIZE;
    
    // calculate the size the buddy structures will take
    get_buddy_size(n_chunks, &height, &struct_size);
    trace("Buddy data for kernel's virtual addr space: %u kiB\n",
      uldivru(struct_size, 1024));
    // TODO: make sure struct_size doesn't exceed the amount of memory mapped
    // after the kernel image at 0xC0000000

    // Here, we can convert a physical address to a virtual address just by
    // adding. This only works during bootstrapping, due to the simple page
    // mapping!
    virtual_buddy = (struct buddy *) ((freemem_low_bound - KERNEL_PHYS_BASE) + KERNEL_VIRT_BASE);

    // initialize the buddy structure
    buddy_init(virtual_buddy, n_chunks, height, struct_size);
    virt_base = KERNEL_VIRT_BASE;

    // Now that we've set up the virtual buddy structure, there's
    // even less free memory.
    struct_size = alignup(struct_size, PAGE_SIZE);
    new_freemem_low_bound = freemem_low_bound + struct_size;

    // Create the physical buddy, to manage all normal physical memory,
    // with a granularity of one page.

    old_struct_size = 0;
    for (;;){
        // how many pages (allocator 'chunks') do we need to manage?
        n_chunks = ((freemem_high_bound - new_freemem_low_bound) 
          - alignup(old_struct_size, PAGE_SIZE)) / PAGE_SIZE;
        // calculate amount of data needed for buddy allocator
        get_buddy_size(n_chunks, &height, &struct_size);
        if (struct_size != old_struct_size){
            // We don't need to manage the memory management data,
            // so we'll calculate the size again, and again, until
            // we settle on an optimum size. This only actually takes
            // about two iterations. There could also be a more
            // linear calculation? :)
            old_struct_size = struct_size;
        } else {
            // the struct_size hasn't changed. Stop iterating.
            break;
        }
    }

    trace("Buddy data for %u MiB physical addr space: %u kiB\n",
      uldivru(freemem_high_bound - freemem_low_bound, 1024 * 1024),
      uldivru(struct_size, 1024));
    // TODO: make sure struct_size doesn't exceed the amount of memory mapped
    // after the kernel image at 0xC0000000
    physical_buddy = (struct buddy *) ((new_freemem_low_bound - KERNEL_PHYS_BASE) 
      + KERNEL_VIRT_BASE);
    phys_base = new_freemem_low_bound + alignup(struct_size, PAGE_SIZE);
    buddy_init(physical_buddy, n_chunks, height, struct_size);
    trace("Buddy allocators initialized.\n");

    // Mark the kernel as used kernel virtual address space (because it is)
    buddy_mark_used(virtual_buddy, 0, uldivru(phys_base - KERNEL_PHYS_BASE, PAGE_SIZE));

    set_up_reflexive_paging();
}

size_t alloc_pages(size_t min_pages, size_t max_pages, intptr_t *start_addr)
{
    unsigned int result;
    size_t allocated_pages;
    result = buddy_alloc(physical_buddy, min_pages, max_pages, &allocated_pages);
    if (result == -1){
        return 0;
    } else {
        *start_addr = phys_base + (result * PAGE_SIZE);
        return allocated_pages;
    }
}

void free_pages(intptr_t start_addr, size_t n_pages)
{
    buddy_free(physical_buddy, (start_addr - phys_base) / PAGE_SIZE, n_pages);
}

size_t alloc_kvpages(size_t min_pages, size_t max_pages, intptr_t *start_addr)
{
    unsigned int result;
    size_t allocated_pages;
    result = buddy_alloc(virtual_buddy, min_pages, max_pages, &allocated_pages);
    if (result == -1){
        return 0;
    } else {
        *start_addr = virt_base + (result * PAGE_SIZE);
        return allocated_pages;
    }
}

void free_kvpages(intptr_t start_addr, size_t n_pages)
{
    buddy_free(virtual_buddy, (start_addr - virt_base) / PAGE_SIZE, n_pages);
}

size_t alloc_kvmem(size_t min_pages, size_t max_pages, void **start_addr, intptr_t *phys_addresses)
{
    // TODO: call alloc_pages repeatedly to try to get up to max_pages

    intptr_t phys_addr, virt_addr;
    size_t n_phys, n_virt;
    unsigned int i;

    // allocate physical pages
    n_phys = alloc_pages(min_pages, max_pages, &phys_addr);
    if (n_phys == 0){
        // failed
        return 0;
    }
    // allocate virtual pages to match
    n_virt = alloc_kvpages(min_pages, n_phys, &virt_addr);
    if (n_virt == 0){
        // failed
        free_pages(phys_addr, n_phys);
        return 0;
    } else if (n_virt < n_phys){
        // free some of our physical pages
        // XXX: this assumes free_pages won't mind freeing half an
        // allocated region. For my buddy allocator, this is true.
        free_pages(phys_addr + n_virt * PAGE_SIZE, n_phys - n_virt);
        n_phys = n_virt;
    }
    // store the physical addresses
    if (phys_addresses){
        for (i=0; i<n_phys; i++){
            phys_addresses[i] = phys_addr + i * PAGE_SIZE;
        }
    }
    // perform the mapping
    if (map_mem(phys_addr, virt_addr, n_phys, PTE_PRESENT | PTE_WRITABLE) != 0){
        // oh dear - couldn't even map :(
        free_pages(phys_addr, n_phys);
        free_kvpages(virt_addr, n_virt);
        return 0;
    }
    *start_addr = (void *) virt_addr;
    return n_phys;
}

void free_kvmem(void *start_addr, size_t n_pages)
{
    // free a physical page at a time, getting the address
    // out the page table
    // XXX: this assumes free_pages won't mind freeing half an
    // allocated region. For my buddy allocator, this is true.
    intptr_t virt_addr = (intptr_t) start_addr;
    intptr_t phys_addr;
    size_t i;

    for (i=0; i<n_pages; i++){
        // fetch physical address and free it
        phys_addr = PAGE_TAB[virt_addr / PAGE_SIZE] & PTE_ADDRESS;
        free_pages(phys_addr, 1);
        // next page
        virt_addr += PAGE_SIZE;
    }
    // now we can perform an unmap
    map_mem(0, (intptr_t) start_addr, n_pages, 0);
    // and finally, free the virtual pages
    free_kvpages((intptr_t) start_addr, n_pages);
}

