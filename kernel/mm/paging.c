#include "mm/paging.h"
#include "mm/mm.h"
#include "console.h" // for debug messages
#include "assert.h"
#include "stdlib.h" // for malloc!

// Keep a list of all page directories, so that we can update the kernel part
// of all of them at once.
static struct pagedir *pagedir_first = NULL, *pagedir_last = NULL;

void set_up_reflexive_paging(void)
{
    intptr_t pagedir_phys;
    pde_t *pagedir_virt;

    // Get the physical address of the page directory.
    // The CPU knows where this is ;)
    pagedir_phys = get_cr3();

    // Since this was the initial page directory, it was
    // allocated just after the kernel and modules, so we
    // can easily find the virtual address for it.
    pagedir_virt = (pde_t *) ((pagedir_phys - KERNEL_PHYS_BASE) + KERNEL_VIRT_BASE);

    // Now we can enable the reflexive mapping
    pagedir_virt[1023] = (pagedir_phys & PDE_ADDRESS) | (PDE_PRESENT | PDE_WRITABLE);

    // No need to flush TLB now, because nothing was mapped there before.
}

int pagedir_create(struct pagedir *page_directory)
{
    size_t n_pages;

    // allocate the actual page directory (a whole page)
    // XXX: these casts are horrible. There must be a cleaner way!
    n_pages = alloc_kvmem(1, 1, (void **) (void *) &page_directory->virt_addr,
      &page_directory->phys_addr);
    if (n_pages == 0){
        trace("pagedir_create: alloc_kvmem failed\n");
        return 1;
    }
    // clear user part of page directory
    memset(page_directory->virt_addr, 0, 768 * sizeof(pde_t));
    // construct the kernel part of the page directory from the current
    // page directory. Since the kernel part is kept consistent, it doesn't
    // really matter where we get this from.
    memcpy(page_directory->virt_addr + 768, PAGE_DIR + 768, 255 * sizeof(pde_t));
    // set up reflexive mapping for this page directory
    page_directory->virt_addr[1023] = (page_directory->phys_addr & PDE_ADDRESS) | (PDE_PRESENT | PDE_WRITABLE);
    // add this page directory to our list, so the kernel part of it gets update
    // in the future.
    page_directory->prev = NULL;
    page_directory->next = pagedir_first;
    if (pagedir_first){
        pagedir_first->prev = page_directory;
    } else {
        pagedir_last = page_directory;
    }
    pagedir_first = page_directory;

    return 0;
}

void pagedir_destroy(struct pagedir *pagedir)
{
    // remove it from the list
    if (pagedir->prev){
        pagedir->prev->next = pagedir->next;
    } else {
        pagedir_first = pagedir->next;
    }
    if (pagedir->next){
        pagedir->next->prev = pagedir->prev;
    } else {
        pagedir_last = pagedir->prev;
    }
    // free the page directory (TODO: make sure it's not
    // currently in use!)
    free_kvmem(pagedir->virt_addr, 1);
}

// create page table N (0..1023) in current address space,
// if it doesn't already exist.
// return 0=ok, 1=error
static int create_pagetab(int n)
{
    // check page directory entry - does the page table exist?
    pde_t pde = PAGE_DIR[n];
    intptr_t new_pagetable;
    size_t n_allocated;
    struct pagedir *pagedir;

    if (pde & PDE_PRESENT){
        // it's already there. Do nothing.
        return 0;
    } else {
        // allocate a new page table
        n_allocated = alloc_pages(1, 1, &new_pagetable);
        if (n_allocated == 0){
            trace("create_pagedir: memory full\n");
            return 1;
        }
        assert(n_allocated == 1); // we only requested 1
        if (n < 768){
            // page table is for the user part of the address space
            PAGE_DIR[n] = new_pagetable | PDE_PRESENT | PDE_WRITABLE | PDE_USER;
        } else {
            // page table is for the kernel part of the address space
            // update all page directories!
            pagedir = pagedir_first;
            while (pagedir){
                pagedir->virt_addr[n] = new_pagetable | PDE_PRESENT | PDE_WRITABLE;
                pagedir = pagedir->next;
            }
        }
        // there was nothing there before, so we don't need a TLB flush
        return 0;
    }
}

int map_mem(intptr_t physical, intptr_t virtual, size_t n_pages, int flags)
{
    intptr_t pagetab_n, old_pagetab_n = -1;
    int must_flush = 0;
    while (n_pages){
        // get page table number for this address
        pagetab_n = virtual / (PAGE_SIZE * 1024);
        // is this address managed by a different page table?
        if (pagetab_n != old_pagetab_n){
            if (create_pagetab(pagetab_n) != 0){
                trace("map_mem: create_pagetab failed\n");
                // XXX: cleanup?
                return 1;
            }
        }
        // is the page present?
        if (PAGE_TAB[virtual / PAGE_SIZE] & PTE_PRESENT){
            // the page is already present, so we need
            // to flush the TLB. Currently only
            // global flushes are used.
            must_flush = 1;
        }
        // set the page table entry
        PAGE_TAB[virtual / PAGE_SIZE] = (physical & PTE_ADDRESS)
          | flags;
        // move on to next page
        virtual += PAGE_SIZE;
        physical += PAGE_SIZE;
        n_pages--;
    }
    if (must_flush){
        trace("Flushing TLB...\n");
        set_cr3(get_cr3());
    }
    return 0; // all is well
}

static struct pagedir init_pagedir;

void paging_init(void)
{
    struct pagedir *pagedir;
    pagedir = &init_pagedir;
    pagedir->phys_addr = get_cr3();
    pagedir->virt_addr = (pde_t *) (
      (pagedir->phys_addr - KERNEL_PHYS_BASE) + KERNEL_VIRT_BASE);
    // make sure no other paging functions have been called
    // before paging_init
    assert(pagedir_first == NULL && pagedir_last == NULL);
    pagedir_first = pagedir_last = pagedir;
}

void pagedir_switch(struct pagedir *pagedir)
{
    set_cr3(pagedir->phys_addr);
}
