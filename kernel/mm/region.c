#include "mm/region.h"
#include "mm/paging.h"
#include "mm/mm.h"
#include "stddef.h"
#include "stdlib.h"
#include "interrupt.h"
#include "console.h" // for debugging
#include "assert.h"

static struct addr_space *current_addr_space = NULL;

static struct p_region *p_r_create(size_t n_pages)
{
    struct p_region *p_r;
    intptr_t paddr_start;
    size_t n_pages_alloc;

    p_r = malloc(sizeof *p_r);
    if (!p_r){
        trace("p_r_create: memory full\n");
    } else {
        n_pages_alloc = alloc_pages(n_pages, n_pages, &paddr_start);
        if (n_pages_alloc < n_pages){
            trace("p_r_create: alloc_pages failed\n");
            free(p_r);
            p_r = NULL;
        } else {
            p_r->ref_count = 1;
            p_r->flags = 0;
            p_r->start = paddr_start / PAGE_SIZE;
            p_r->len = n_pages;
        }
    }
    return p_r;
}

static struct p_region *p_r_create_fixed(intptr_t physical, size_t n_pages)
{
    struct p_region *p_r;
    
    p_r = malloc(sizeof *p_r);
    if (!p_r){
        trace("p_r_create_fixed: memory full\n");
    } else {
        p_r->ref_count = 1;
        p_r->flags = 0;
        p_r->start = physical / PAGE_SIZE;
        p_r->len = n_pages;
    }
    return p_r;
}

static void p_region_addref(struct p_region *p_r)
{
    // TODO: bounds check
    p_r->ref_count++;
}

static void p_region_release(struct p_region *p_r)
{
    p_r->ref_count--;
    if (p_r->ref_count == 0){
        // no longer used - free it
        free_pages(p_r->start * PAGE_SIZE, p_r->len);
        free(p_r);
    }
}

// create a vm_region and insert it into the address space
// (does not deal with corresponding p_region)
// TODO: check for overlapping regions? (which wouldn't make sense)
static struct vm_region *add_vm_region(struct addr_space *aspace, intptr_t virtual, size_t n_pages)
{
    struct vm_region *vm_r;
    vm_r = malloc(sizeof *vm_r);
    if (!vm_r){
        trace("add_vm_region: memory full\n");
        return NULL;
    }
    vm_r->start = virtual / PAGE_SIZE;
    vm_r->len = n_pages;
    vm_r->flags = 0;
    // linked-list prepend
    vm_r->prev = NULL;
    vm_r->next = aspace->regions;
    if (aspace->regions){
        aspace->regions->prev = vm_r;
    }
    aspace->regions = vm_r;
    return vm_r;
}

struct addr_space *addr_space_new(void)
{
    struct addr_space *aspace;
    aspace = malloc(sizeof *aspace);
    if (!aspace){
        trace("new_addr_space: memory full\n");
    } else {
        aspace->regions = NULL;
        pagedir_create(&aspace->pagedir); // TODO: check return val!
    }
    return aspace;
}

void addr_space_delete(struct addr_space *aspace)
{
    // delete and remove each region
    struct vm_region *vm_r, *vm_r_next;
    vm_r = aspace->regions;
    while (vm_r){
        vm_r_next = vm_r->next;
        if (vm_r->p_region){
            p_region_release(vm_r->p_region);
        }
        free(vm_r);
        vm_r = vm_r_next;
    }
    pagedir_destroy(&aspace->pagedir);
    // free the addr_space struct
    free(aspace);
}

struct vm_region *vm_region_new_physical(struct addr_space *aspace,
  intptr_t virtual, size_t n_pages)
{
    struct vm_region *vm_r;
    struct p_region *p_r;

    vm_r = add_vm_region(aspace, virtual, n_pages);
    if (!vm_r){
        return NULL;
    }
    p_r = p_r_create(n_pages);
    if (!p_r){
        // TODO: remove virtual region!!
        return NULL;
    }
    vm_r->p_region = p_r;
    return vm_r;
}

struct vm_region *vm_region_new_physical_fixed(struct addr_space *aspace,
  intptr_t virtual, intptr_t physical, size_t n_pages)
{
    struct vm_region *vm_r;
    struct p_region *p_r;

    vm_r = add_vm_region(aspace, virtual, n_pages);
    if (!vm_r){
        return NULL;
    }
    p_r = p_r_create_fixed(physical, n_pages);
    if (!p_r){
        // TODO: remove virtual region!!
        return NULL;
    }
    vm_r->p_region = p_r;
    return vm_r;
}

struct vm_region *vm_region_new_physical_shared(struct addr_space *aspace,
  intptr_t virtual, size_t n_pages)
{
    struct vm_region *vm_r;
    vm_r = vm_region_new_physical(aspace, virtual, n_pages);
    if (vm_r){
        vm_r->p_region->flags |= P_R_SHARED;
    }
    return vm_r;
}

void vm_region_remove(struct addr_space *aspace, struct vm_region *vm_r)
{
    // linked-list remove
    if (vm_r->prev){
        vm_r->prev->next = vm_r->next;
    } else {
        aspace->regions = vm_r->next;
    }
    if (vm_r->next){
        vm_r->next->prev = vm_r->prev;
    }

    // release the associated physical memory
    if (vm_r->p_region){
        p_region_release(vm_r->p_region);
    }
    // free the struct
    free(vm_r);
}

struct vm_region *vm_region_dup_shared(struct addr_space *new_addr_space, struct vm_region *vm_r, intptr_t virtual)
{
    struct vm_region *new_vm_r;

    new_vm_r = add_vm_region(new_addr_space, virtual, vm_r->len);
    if (!new_vm_r){
        return NULL;
    }
    new_vm_r->p_region = vm_r->p_region;
    new_vm_r->flags = vm_r->flags;
    p_region_addref(new_vm_r->p_region);
    return new_vm_r;
}

// find vm_region which contains address 'virtual'
static struct vm_region *vm_region_find(struct addr_space *aspace, intptr_t virtual)
{
    struct vm_region *vm_r;
    // linear search!
    vm_r = aspace->regions;
    while (vm_r){
        if (vm_r->start <= (virtual / PAGE_SIZE)
          && (vm_r->start + vm_r->len) > (virtual / PAGE_SIZE)){
            return vm_r;
        } else {
            vm_r = vm_r->next;
        }
    }
    // no such luck
    return NULL;
}

void addr_space_switch(struct addr_space *new_addr_space)
{
    current_addr_space = new_addr_space;
    pagedir_switch(&new_addr_space->pagedir);
}

void pagefault_handler(int vector, struct interrupt_stack *is)
{
    struct vm_region *vm_r;
    struct p_region *p_r;
    intptr_t fault_addr, error_code;

    fault_addr = get_cr2();
    error_code = is->error_code;

    assert(current_addr_space != NULL);
    vm_r = vm_region_find(current_addr_space, fault_addr);
    if (!vm_r){
        panic("Page fault at address 0x%.8X - nothing there\n",
          fault_addr);
    } else {
        p_r = vm_r->p_region;
        assert(p_r->len == vm_r->len); // sanity check
        if (error_code & 0x1){
            // page fault caused by page-level protection violation
            // i.e. the page was there, but permissions were wrong.
            if (!(vm_r->flags & VM_R_WRITABLE)){
                panic("Page fault at address 0x%.8X - tried to write to read-only region",
                  fault_addr);
            } else {
                panic("COW NOT IMPLEMENTED");
            }
        //} else if (!(error_code & 0x4)){
            // page fault caused by kernel
        } else if (error_code & 0x8){
            panic("Page fault caused by reserved bit violation");
        } else {
            // page not present - time to map it
            int map_flags;
            map_flags = PTE_PRESENT | PTE_USER;
            // set the writable flag in the page table
            if ((vm_r->flags & VM_R_WRITABLE) &&
              !(p_r->ref_count > 1 && !(p_r->flags & P_R_SHARED))){ // if the region isn't shared,
                                                                    // and it's referenced by >1 vm_region,
                                                                    // don't make it writable, so that we can
                                                                    // handle the page fault.
                map_flags |= PTE_WRITABLE;
            }
            trace("Mapping 0x%.8X to 0x%.8X (%d)\n",
              vm_r->start * PAGE_SIZE,
              p_r->start * PAGE_SIZE,
              vm_r->len);
            map_mem(p_r->start * PAGE_SIZE,         // physical
              vm_r->start * PAGE_SIZE,              // virtual
              vm_r->len,                            // n_pages
              map_flags);                           // flags
        }
    }
}

void set_pagefault_handler(void)
{
    interrupt_set_handler(0xE, pagefault_handler);
}

