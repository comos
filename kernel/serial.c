#include "serial.h"
#include "portio.h"

struct serial com1, com2, com3, com4;

#define COM1_IRQ 4
#define COM2_IRQ 3
#define COM1_BASE 0x3F8
#define COM2_BASE 0x2F8
#define COM3_BASE 0x3E8
#define COM4_BASE 0x2E8

static int detect_uart(struct serial *self);
static char sanitise_output(struct serial *self, char ch);

// Probe to see if the device is there
// 0 - no UART
// 1 - 8250
// 2 - 16450 or 8250 with scratch reg
// 3 - 16550
// 4 - 16550A
static int detect_uart(struct serial *self)
{
    unsigned int x, tmp;

    // Check if UART is present
    tmp = inb(self->port + 4);
    outb(self->port + 4, 0x10);
    if ((inb(self->port + 6) & 0xF0))
        return 0;
    outb(self->port + 4, 0x1F);
    if ((inb(self->port + 6) & 0xF0) != 0xF0)
        return 0;
    outb(self->port + 4, tmp);

    // Look for scratch register
    tmp = inb(self->port + 7);
    outb(self->port + 7, 0x55);
    if (inb(self->port + 7) != 0x55)
        return 1;
    outb(self->port + 7, 0xAA);
    if (inb(self->port + 7) != 0xAA)
        return 1;
    outb(self->port + 7, tmp);

    // Look for a FIFO
    outb(self->port + 2, 1);
    x = inb(self->port + 2);
    outb(self->port + 2, 0x00);
    if ((x & 0x80) == 0)
        return 2;
    if ((x & 0x40) == 0)
        return 3;
    return 4;
}

static char sanitise_output(struct serial *self, char ch)
{
    if (ch == '\n'){
        // We have to send the age-old CRLF combo
        // for a newline
        serial_putchar(self, '\r');
        return '\n';
    } else if (ch == '\b'){
        return 0;
    } else {
        return ch;
    }
}

// send a single character
void serial_putchar(struct serial *self, char ch)
{
    // remove characters we don't want (control characters, etc.)
    if (!(ch = sanitise_output(self, ch)))
        return;
    // wait for UART to be ready
    // TODO: should we yield while waiting?
    while (!(inb(self->port + 5) & 0x20)){
        // do nothing
    }
    // send!
    outb(self->port, ch);
}

int serial_init(struct serial *self, int com_number, unsigned long baud)
{
    unsigned int divisor;

    switch (com_number){
        case 1:
            self->port = COM1_BASE;
            self->irq = COM1_IRQ;
            break;
        case 2:
            self->port = COM2_BASE;
            self->irq = COM2_IRQ;
            break;
        case 3:
            self->port = COM3_BASE;
            self->irq = COM1_IRQ;
            break;
        case 4:
            self->port = COM4_BASE;
            self->irq = COM2_IRQ;
            break;
        default:
            return 1;
    }

    if (detect_uart(self) == 0){
        // couldn't actually find the device
        return 1;
    }

    divisor = 115200 / baud;
    outb(self->port + 1, 0x00); // disable all interrupts
    outb(self->port + 3, 0x80); // enable 'DLAB' - baud rate divisor
    outb(self->port + 0, divisor); // divisor (lower)
    outb(self->port + 1, divisor >> 8); // divisor (upper)
    outb(self->port + 3, 0x03); // 8-bits, no parity, one stop bit
    outb(self->port + 2, 0xC7); // enable FIFO, clear them, with 14-byte threshold
    outb(self->port + 4, 0x0B); // enable (something)
    outb(self->port + 4, inb(self->port + 4) | 8); // set OUT2 bit to enable interrupts
    outb(self->port + 1, 0x01); // enable ERBFI (receiver buffer full) interrupt

    self->initialized = true;
    return 0;
}

void serial_puts(struct serial *self, const char *str)
{
    while (*str){
        serial_putchar(self, *str);
        str++;
    }
}
