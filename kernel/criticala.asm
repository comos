bits 32

;;;; Assembly routines to compliment critical.c

global critical_enter_internal
critical_enter_internal:
    ; push the flags register
    pushf
    ; disable interrupts
    cli
    ; return the previous state of the interrupt flag
    pop   edx
    xor   eax,eax
    test  edx,0x0200
    setz  al
    ret
