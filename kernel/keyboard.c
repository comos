#include "keyboard.h"
#include "portio.h"
#include "console.h"
#include "interrupt.h"
#include "general.h"
#include "critical.h"
#include "shell.h"

kbListener		kbList[255] = {0};
uint8_t 		listpos = 0;
static unsigned int kbd_state = KBD_NUMLOCK_STATE, kbd_led_state = 0;

unsigned char lowercase[128] =
{
    0,0,'1','2','3','4','5','6','7','8','9','0','-','+','\b','\t','q','w','e','r','t','y','u','i','o','p','[',']','\n',
    0,'a','s','d','f','g','h','j','k','l',';','\'','`',0,'\\','z','x','c','v','b','n','m',',','.','/',0,0,0,' ',0,
    
    [59] = K_F1, [60] = K_F2, [61] = K_F3, [62] = K_F4, [63] = K_F5, [64] = K_F6, [65] = K_F7, 
    [66] = K_F8, [67] = K_F9, [68] = K_F10, [69] = K_F11, [70] = K_F12,
    [72] = K_UP, [75] = K_LEFT, [77] = K_RIGHT, [80] = K_DOWN,
};  

unsigned char uppercase[128] =
{
    0,0,'!','@','#','$','%','^','&','*','(',')','_','+','\b','\t','Q','W','E','R','T','Y','U','I','O','P','[',']','\n',
    0,'A','S','D','F','G','H','J','K','L',':','"','~',0,'|','Z','X','C','V','B','N','M','<','>','?',0,0,0,' ',0,
    
    [59] = K_F1, [60] = K_F2, [61] = K_F3, [62] = K_F4, [63] = K_F5, [64] = K_F6, [65] = K_F7, 
    [66] = K_F8, [67] = K_F9, [68] = K_F10, [69] = K_F11, [70] = K_F12,
    [72] = K_UP, [75] = K_LEFT, [77] = K_RIGHT, [80] = K_DOWN,
};

// Wait for the keyboard to be ready
void kbd_wait(void)
{
    uint8_t status;
    do {
        status = inb(0x64);
    } while (status & 0x2);
}

// Set keyboard LEDs
void kb_set_leds(int led_states)
{
    trace("kb_set_leds\n");
    kbd_wait();
    // Tell the keyboard controller that we want to modify the LEDs
    outb(0x60, 0xED);
    kbd_wait();
    outb(0x60, (led_states / KBD_CAPSLOCK_STATE) & 0x7);
    kbd_wait();
    kbd_led_state = led_states;
}

/* Handles the keyboard interrupt */
static void kb_int_handler(int vector, struct interrupt_stack *is)
{
	uint8_t new_scan_code = inb(0x60);
    uint8_t new_char;
    uint8_t x;
    bool handled;
    
    switch(new_scan_code) {       
        case 29:                /* CTRL */
            kbd_state |= KBD_CTRL_STATE;
            break;
        case (29 + (1 << 7)):
            kbd_state &= ~KBD_CTRL_STATE;
            break;
            
        case 56:                /* ALT */
            kbd_state |= KBD_ALT_STATE;
            break;
        case (56 + (1 << 7)):
            kbd_state &= ~KBD_ALT_STATE;
            break;
        
        case 58:                /* CAPS LOCK */
            kbd_state ^= KBD_CAPSLOCK_STATE;
            break;
        
        case 69:                /* NUM LOCK */
            kbd_state ^= KBD_NUMLOCK_STATE;
            break;
            
        case 70:                /* SCROLL LOCK */
            kbd_state ^= KBD_SCROLLLOCK_STATE;
            break;
            
        case 42:                /* SHIFT */
        case 54:
            kbd_state |= KBD_SHIFT_STATE;
            break;
        case (42 + (1 << 7)):
        case (54 + (1 << 7)):
            kbd_state &= ~KBD_SHIFT_STATE;
            break;

        default:
            /* keep parsing if new_scan_code is not a break code */
            if (!(new_scan_code & 0x80))
            {
                new_char = (kbd_state & KBD_SHIFT_STATE ? uppercase : lowercase)
                  [new_scan_code];

                handled = false;
                
                // virtual terminal switching
                // this isn't really the place to do it...
                if (kbd_state & KBD_ALT_STATE){
                    if (new_char >= '1' && new_char <= '4'){
                        struct vterm *vt;
                        vt = get_vterm(new_char - '1');
                        if (vt){
                            switch_vterm(vt);
                            handled = true;
                        }
                    }
                }

                if (!handled){
                    /* distribute new_char to our listeners */
                    for (x = 0; x < listpos; x++)
                    {
                        kbList[x](new_char);
                    }
                }
            }
            break;
    }
    
    /* if our saved led state does not match our new state, change some lights =) */
    if ((kbd_state & (KBD_CAPSLOCK_STATE | KBD_NUMLOCK_STATE | KBD_SCROLLLOCK_STATE))
      != kbd_led_state){
        kb_set_leds(kbd_state & (KBD_CAPSLOCK_STATE | KBD_NUMLOCK_STATE | KBD_SCROLLLOCK_STATE));
    }

	/* Acknowledge the IRQ, pretty much tells the PIC that we can accept >= priority IRQs now. */
    ack_irq(1);
}

void keyboard_init(void)
{
    interrupt_set_handler(irq_to_int(1), &kb_int_handler);
    unmask_irq(1);
}

void keyboard_register_listener(kbListener nlist)
{
	kbList[listpos++] = nlist;
}
