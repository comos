bits 32

global enter_task
enter_task:
    ; (struct pusha_struct *gpr, uint32_t eip, uint32_t eflags)
    mov   eax,[esp+4]       ; gpr
    mov   ecx,[esp+8]       ; eip
    mov   edx,[esp+12]      ; eflags

    ; push the iret frame
    push  dword 0x23        ; SEG_DPL3_DATA (ss)
    push  dword [eax+12]    ; esp
    push  edx               ; eflags
    push  dword 0x1B        ; SEG_DPL3_CODE (cs)
    push  ecx               ; eip
    
    ; load all general purpose registers
    ; (except esp)
    mov   edi,[eax]
    mov   esi,[eax+0x4]
    mov   ebp,[eax+0x8]
    ;;;   skip esp
    mov   ebx,[eax+0x10]
    mov   edx,[eax+0x14]
    mov   ecx,[eax+0x18]
    mov   eax,[eax+0x1C]

    ; iret frame set up...
    ; let's go!
    iret

dead_code:
    ; this is the old, kernel-task-switching code
    ; that will be used again soon
    ; (struct pusha_struct *gpr, uint32_t eip, uint32_t eflags)
    cli
    mov eax,[esp+4] ; gpr
    mov ecx,[esp+8] ; eip
    mov edx,[esp+12] ; eflags
    mov esp,[eax+12] ; load esp
    push edx ; push eflags
    ; push dword 0x08 ; push SEG_DPL0_CODE
    push dword 0x1B ; push SEG_DPL3_CODE
    push ecx ; push eip
    push dword [eax+28] ; push GPRs
    push dword [eax+24]
    push dword [eax+20]
    push dword [eax+16]
    push dword [eax+12]
    push dword [eax+8]
    push dword [eax+4]
    push dword [eax]
    popa
    ; sti - not needed - iret loads eflags
    iret
