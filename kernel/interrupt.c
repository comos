#include "interrupt.h"
#include "gdt.h"
#include "portio.h"
#include "console.h"
#include "general.h"

/* PIC registers */
#define PIC1_CMD 0x20
#define PIC1_DATA 0x21
#define PIC2_CMD 0xA0
#define PIC2_DATA 0xA1

/* PIC commands */
#define PIC_ACK 0x20
#define ICW1_ICW4 0x01
#define ICW1_SINGLE 0x02
#define ICW1_INTERVAL4 0x04
#define ICW1_LEVEL 0x08
#define ICW1_INIT 0x10
#define ICW4_8086 0x01
#define ICW4_AUTO 0x02
#define ICW4_BUF_SLAVE 0x08
#define ICW4_BUF_MASTER 0x0C
#define ICW4_SFNM 0x10

static uint16_t idt[256 * 4]; // the actual IDT that the CPU sees
extern uint32_t isr_table[]; // defined in isr.asm - points to all the ISRs

// this is the default interrupt handler, so we can see
// if an interrupt fired that shouldn't have.
static void default_interrupt_handler(int vector, struct interrupt_stack *is)
{
    console_printf(get_vterm(0), "Interrupt 0x%X fired.\n", vector);
    console_printf(get_vterm(0), "Return address: 0x%.8X\n", is->eip);
    if (vector >= 0x20 && vector < 0x30){
        ack_irq(vector - 0x20);
    } else if (vector <= 19){
        panic("Unhandled exception!");
    }
}

// tell the CPU where the IDT is. Note that
// it's convenient to consider the IDT
// as a uint16_t array.
static void load_idt_addr(uint16_t *idt)
{
    asm volatile (
      "subl $6,%%esp        \n"
      "movw %%cx,(%%esp)    \n"
      "movl %%eax,2(%%esp)  \n"
      "lidt (%%esp)         \n"
      "addl $6,%%esp        \n"
      :: "c" (256 * 8), "a" (idt)
      : "cc");
}

// small busy-wait loop for configuring the PIC
static void io_wait(void)
{
    int i;
    for (i=0; i<16; i++)
        outb(0x80, 0x00);
}

// remap PICs to use 0x20 and 0x28 as base vectors
static void remap_pics(void)
{
    int pic1_offset = 0x20;
    int pic2_offset = 0x28;
    unsigned char a1, a2;

    //a1 = inb(PIC1_DATA);
    //a2 = inb(PIC2_DATA);
    a1 = 0xFF;
    a2 = 0xFF;

    outb(PIC1_CMD, ICW1_INIT + ICW1_ICW4);
    io_wait();
    outb(PIC2_CMD, ICW1_INIT + ICW1_ICW4);
    io_wait();
    outb(PIC1_DATA, pic1_offset);
    io_wait();
    outb(PIC2_DATA, pic2_offset);
    io_wait();
    outb(PIC1_DATA, 4);
    io_wait();
    outb(PIC2_DATA, 2);
    io_wait();
    outb(PIC1_DATA, ICW4_8086);
    io_wait();
    outb(PIC2_DATA, ICW4_8086);
    io_wait();
    outb(PIC1_DATA, a1);
    outb(PIC2_DATA, a2);
}

void interrupt_init(void)
{
    int i;
    uint32_t isr_addr;
    
    default_handler_ptr = (uint32_t)default_interrupt_handler;
    num_installed_interrupts = 0;
    
    // fill in the IDT
    for (i=0; i<256; i++){
        isr_addr = isr_table[i];
        idt[i * 4 + 0] = isr_addr;
        idt[i * 4 + 1] = SEG_DPL0_CODE; // use the ring-0 code segment for interrupt handler
        //idt[i * 4 + 2] = 0xEE00; // interrupt gate descriptor
        idt[i * 4 + 2] = 0x8E00; // interrupt gate descriptor
        idt[i * 4 + 3] = isr_addr >> 16;
        // set the default interrupt handler, too
        interrupt_handlers[i] = default_interrupt_handler;
    }
    // load the IDT address
    load_idt_addr(idt);

    // remap the PICs
    remap_pics();

    // enable the cascade irq (so that the slave PIC can work,
    // and we can service IRQs 8-15)
    unmask_irq(2);

}

// set up an interrupt handler for interrupt 'vector'
void interrupt_set_handler(int vector, isr_t handler)
{
    num_installed_interrupts++;
    interrupt_handlers[vector] = handler;
}

// remove an interrupt handler (set it to the default handler)
void interrupt_clear_handler(int vector)
{
    if(num_installed_interrupts){num_installed_interrupts--;}
    interrupt_handlers[vector] = default_interrupt_handler;
}

void interrupt_set_flags(int vector, int flags)
{
    if (flags & INTR_USER){
        idt[vector * 4 + 2] = 0xEE00;
    } else {
        idt[vector * 4 + 2] = 0x8E00;
    }
}

// get an interrupt vector from an IRQ
int irq_to_int(int irq)
{
    return irq + 0x20;
}

// get the irq number behind an interrupt vector
int int_to_irq(int vector)
{
    return vector - 0x20;
}

// unmask (enable) IRQ n
void unmask_irq(int n)
{
    unsigned char a;
    if (n < 8){
        // master PIC
        a = inb(PIC1_DATA);
        a &= ~(1 << n);
        outb(PIC1_DATA, a);
    } else {
        // slave PIC
        a = inb(PIC2_DATA);
        a &= ~(1 << (n - 8));
        outb(PIC2_DATA, a);
    }
}

// mask (disable) IRQ n
void mask_irq(int n)
{
    unsigned char a;
    if (n < 8){
        // master PIC
        a = inb(PIC1_DATA);
        a |= (1 << n);
        outb(PIC1_DATA, a);
    } else {
        // slave PIC
        a = inb(PIC2_DATA);
        a |= (1 << (n - 8));
        outb(PIC2_DATA, a);
    }
}

// acknowledge IRQ n
void ack_irq(int n)
{
    if (n >= 8)
        outb(PIC2_CMD, PIC_ACK);
    outb(PIC1_CMD, PIC_ACK);
}

