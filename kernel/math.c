#include "console.h"
#include "stddef.h"
#include "cpuid.h"

#define CR0_EM          (1 << 2)
#define CR0_MP          (1 << 1)
#define CR4_OSFXSR      (1 << 9)
#define CR4_OSXMMEXCPT  (1 << 10)

void init_fpu(void)
{
    /* FINIT = FPU INIT.  This allows for the use of Float/Double/Long Double data types */
    if(cpuid.features_edx & 0x01) /* checks for the FPU flag */ 
    {
        asm volatile("finit;");     
    }
}

void init_sse(void)
{
    uint32_t cr0, cr4;
    
    /* Set MP, OSFXSR, and OSXMMEXCPY, and disable EM (FPU emulation) */
    if(cpuid.features_edx & 0x06000001) /* test for SSE2, SSE, and FPU */
    {
        asm volatile("movl %%cr0, %0;":"=r"(cr0)); /* store CR0 */
        asm volatile("movl %%cr4, %0;":"=r"(cr4)); /* store CR4 */
        
        cr0 &= ~CR0_EM; /* disable FPU emulation */
        cr0 |= CR0_MP;  /* set MP bit */
        cr4 |= (CR4_OSFXSR | CR4_OSXMMEXCPT); /* set the bits to enable FPU SAVE/RESTORE and XMM registers */
        
        asm volatile("movl %0, %%cr4;"::"r"(cr4)); /* restore CR4 */
        asm volatile("movl %0, %%cr0;"::"r"(cr0)); /* restore CR0 */
    }   
}
