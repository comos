#include "cpu.h"
#include "general.h"
#include "console.h"
#include "cpuid.h"
#include "mm/mm.h"
#include "mm/paging.h"

static const intptr_t APIC_BASE = 0xFEE00020;
static intptr_t apic_virt;
enum {
    APIC_REG_ID = 0x20,
    APIC_REG_VERSION = 0x30,
    APIC_REG_TPR = 0x80,
    APIC_REG_APR = 0x90,
    APIC_REG_PPR = 0xA0,
    APIC_REG_EOI = 0xB0,
    APIC_REG_SPUR = 0xF0,
};

void write_apic_register(const uint16_t offset, const uint32_t val)
{*(volatile uint32_t*)(apic_virt + offset) = val;}

uint32_t read_apic_register(const uint16_t offset)
{return *(volatile uint32_t*)(apic_virt + offset);}

/* bits 24-27 (P6) or 24-31 (P4/Xeon) are for the APIC ID */
uint8_t get_cpu_id(void)
{return (uint8_t)((read_apic_register(APIC_REG_ID) >> 24) & 0xFF);}

void apic_init(void)
{
    // Allocate a page of kernel virtual address space
    if (alloc_kvpages(1, 1, &apic_virt) < 1){
        panic("cannot allocate (alloc_kvpages) APIC area");
    }
    // Map 0xFEE00000 .. 0xFEE00FFF
    if (map_mem(APIC_BASE & ~PAGE_SIZE, apic_virt, 1, PTE_PRESENT | PTE_WRITABLE)){
        panic("cannot map APIC area");
    }
    // But the APIC area starts at 0xFEE00020
    apic_virt += 0x20;
}

void enable_local_apic(void)
{
    uint8_t enabled_via_msr;
    
    if (cpuid.features_edx & CPUID_FEAT_EDX_MSR){
        /* try enabled the APIC in MSR 0x1B */
        asm volatile("mov $0x1b, %ecx;  "
                     "rdmsr;            "
                     "or $0x800, %eax;  "
                     "mov $0x1b, %ecx;  "
                     "wrmsr;            ");

        asm volatile("mov $0x1b, %%ecx; "
                     "bt $11, %%eax;    "
                     "jz .zero;         "
                     "mov $0, %0;       "
                     ".zero:;           "
                     "mov $1, %0;       "
                     :"=r"(enabled_via_msr));
                     
        if(enabled_via_msr)
        {
            console_printf(get_vterm(0), "APIC: software enable in IA32_APIC_BASE MSR succeeded\n");
            return;
        }
    }

    write_apic_register(APIC_REG_SPUR, read_apic_register(APIC_REG_SPUR) | (1 << 8));
    
    console_printf(get_vterm(0), "APIC: software enable in SVR (APIC register 0xF0) ");
    
    if((read_apic_register(APIC_REG_SPUR) & (1 << 8)))
    {
        console_printf(get_vterm(0), "APIC: software enable in SVR (APIC register 0xF0) succeeded\n");
        return;
    }
    
    console_printf(get_vterm(0), "APIC: software enable failed\n");
}
