#include "rtc.h"
#include "stdlib.h"
#include "console.h"
#include "interrupt.h"
#include "portio.h"

static uint8_t read_cmos(const uint8_t reg)
{
    /* we don't to cause any undefined behaviour by reading above CMOS RAM */
    if(reg >= 64){return 0;}
    
    outb(0x70, reg);
    return inb(0x71);
}

static void write_cmos(uint8_t reg, uint8_t val)
{
    /* we don't to cause any undefined behaviour by reading above CMOS RAM */
    if(reg >= 64){return;}
    
    outb(0x70, reg);
    outb(0x71, val);
}

static uint16_t cmos_checksum()
{
    uint8_t i;
    uint16_t checksum;
    
    checksum = 0;
    
    for(i = 16; i < 46; i++)
    {checksum += read_cmos(i);}

    return hs2net_16(checksum);
}

static void rtc_handler(int vector, struct interrupt_stack *is)
{
    time.second = bcd2bin(read_cmos(0x00));
    time.minute = bcd2bin(read_cmos(0x02));
    time.hour =  bcd2bin(read_cmos(0x04));
    time.day_of_week = bcd2bin(read_cmos(0x06));
    time.day_of_month = bcd2bin(read_cmos(0x07));
    time.month = bcd2bin(read_cmos(0x08));
    
    uint8_t year = bcd2bin(read_cmos(0x09));
    uint16_t century = bcd2bin(read_cmos(0x32));
    
    century *= 100;
    time.year = century + year;
    
    read_cmos(0x0C);        /* clear pending interrupts */
    ack_irq(8);
}

static void force_update()
{
    read_cmos(0x0C);        /* clear pending interrupts */
    
    time.second = bcd2bin(read_cmos(0x00));
    time.minute = bcd2bin(read_cmos(0x02));
    time.hour =  bcd2bin(read_cmos(0x04));
    time.day_of_week = bcd2bin(read_cmos(0x06));
    time.day_of_month = bcd2bin(read_cmos(0x07));
    time.month = bcd2bin(read_cmos(0x08));
    
    uint8_t year = bcd2bin(read_cmos(0x09));
    uint16_t century = bcd2bin(read_cmos(0x32));
    
    century *= 100;
    time.year = century + year;
}

void init_rtc()
{
    uint8_t status_b;
    uint16_t theirs, ours;
     
    theirs = read_cmos(46) & 0xFF;
    theirs += ((read_cmos(47) << 8) & 0xFF00);

    ours = cmos_checksum();
    
    /* QEMU seems to not use the checksum field, so we have to take this with a grain of salt. */
    if(theirs != ours)
    {
        console_printf(get_vterm(0), "CMOS: checksum error.  theirs = %x, ours = %x \n", theirs, ours);
    }
    
    /* this, on the other hand, is not to be looked over lightly */
    if(!(read_cmos(0x0D) & 0x80))
    {
        console_printf(get_vterm(0), "CMOS: data invalid (status register D = %x) \n", read_cmos(0x0D));
        return;
    }   
    
    status_b = read_cmos(0x0B);
    status_b |= 0x12;   /* set UI and 24h */
    status_b &= 0x9F;   /* clear PI and AI */
    write_cmos(0x0B, status_b);

    time.second = 0;
    interrupt_set_handler(irq_to_int(8), &rtc_handler);
    unmask_irq(8);
   
    read_cmos(0x0C);        /* clear pending interrupts */
    force_update();
    
    console_printf(get_vterm(0), "RTC: current time = %u:%u:%u, date = %u/%u/%u\n", time.hour, time.minute, time.second, time.month, time.day_of_month, time.year);
}








