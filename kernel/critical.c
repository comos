#include "critical.h"
#include "stddef.h"
#include "stdint.h"
#include "general.h"
#include "mp_tables.h"
#include "cpuid.h"

static struct mp_counts mp;
static volatile uint32_t spinlock = 0;     /* volatile to not optimize the smp_lock loop away */

// defined in criticala.asm
int critical_enter_internal(void);

static void smp_lock(void)
{
    memory_barrier();               /* syncronize */
    asm volatile(
        "1:                         " 
        "    pause;                 " /* used to optimize loops on P4/Xeon's */
        "    lock btsl $0x00, %0;   " /* if no lock is set, it will btsl (bit set long) */ 
        "    jc 1b;                 " /* still locked? */
        ::"m"(spinlock)
        : "cc");
}

static void smp_unlock(void)
{
    spinlock = 0;       /* unlock */
}   

int critical_enter(void)
{
    int state = critical_enter_internal();
    if (state == 1){
        // interrupts *were* enabled
        if(mp.processor_count > 1)
        {
            smp_lock();
        }
    }
    return state;
}

void critical_exit(int state)
{
    if (state == 1){
        if(mp.processor_count > 1){
            smp_unlock();
        }
        sti();
    }
}

/* mb = memory barrier, used for code synchronization */
void memory_barrier(void)
{
    if(cpuid.features_edx & (1 << 26)) /* test for SSE2 */
    {
        asm volatile("mfence");
    }
}

