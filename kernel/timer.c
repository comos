#include "timer.h"
#include "portio.h"
#include "console.h"
#include "interrupt.h"
#include "stdint.h"
#include "general.h"
#include "critical.h"
#include "task.h"

#define TIMER_IRQ 0      // the IRQ the PIT is connected to
#define TIMER_FREQ 1000  // frequency to set the timer to
#define CTL 0x43         // control register
#define CNT0 0x40        // count register 0
#define CNT1 0x41        // count register 1
#define CNT2 0x42        // count register 2

static tick_t tick = 0;    // current tick number
static unsigned long freq; // current timer frequency

tick_t get_tick(void)
{
    return tick;
}

static void timer_interrupt(int vector, struct interrupt_stack *is)
{
    tick++;
    ack_irq(TIMER_IRQ);
    
    if(!(tick % 20))
    {if(MULTITASKING){switch_task(is);}}
}

void set_frequency(unsigned long new_freq)
{
    unsigned short divisor;
    divisor = 1193180 / new_freq;
    freq = (1193180LL << 32) / divisor;
    outb(CTL, 0x36);
    outb(CNT0, divisor);
    outb(CNT0, divisor >> 8);
}

// timer initialization
void timer_init(void)
{
    unsigned char p61h;
    // Timer's base frequency is 1,193,180 Hz
    // We set the frequency divider to get
    // TIMER_FREQ Hz output
    outb(CTL, 0x54);
    outb(CNT1, 18); // LSB only clock divisor (?)
    tick = 0;
    interrupt_set_handler(irq_to_int(TIMER_IRQ), timer_interrupt);
    set_frequency(TIMER_FREQ);
    unmask_irq(TIMER_IRQ);
    
    outb(CTL, 0xB6);
    outb(CNT2, 54);
    outb(CNT2, 124);
    p61h = inb(0x61);
    p61h |= 3;
    outb(0x61, p61h);
}

uint64_t read_cpu_timestamp(void)
{
    uint32_t hi, lo;
    __asm__ __volatile__("rdtsc;":"=d"(hi),"=a"(lo)::"memory");
    return (uint64_t)(((uint64_t)hi << 32) | lo);
}

