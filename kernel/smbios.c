#include "smbios.h"
#include "stdlib.h"
#include "console.h"

static uint8_t smbios_checksum(const intptr_t start, const uint32_t count)
{   
    uint32_t i;
    uint8_t chksum;
    
    chksum = 0;
    for(i = 0; i < count; i++)
    {
        chksum += *(uint8_t*)(start + i);
    }
    
    return chksum;
}


static char * smbios_get_string(intptr_t location, uint8_t id)
{
    char * buf;
    uint8_t counter;

    if(!id)
    {return 0;}

    counter = 0;
    id--;

    while(1)
    {
        if(id == counter)
        {
            buf = (char *)(malloc(strlen((char *)location + 1)));
            memcpy((uint8_t*)buf, (uint8_t*)location, strlen((char *)location) + 1);
            return buf;
        }
        
        while(*(uint8_t*)location++ != 0x00){}
        if(*(uint8_t*)location == 0x00 && *(uint8_t*)(location + 1) == 0x00){break;}
        
        location++;
        counter++;
    }
    
    return 0;
}

static void parse_smbios_memdev(intptr_t location)
{
    memcpy((uint8_t*)&sm_memdev[sm_memdev_count-1], (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_memdev[sm_memdev_count-1].device_locator_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].device_locator);
    sm_memdev[sm_memdev_count-1].bank_locator_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].bank_locator);
    sm_memdev[sm_memdev_count-1].manufacturer_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].manufacturer);
    sm_memdev[sm_memdev_count-1].serial_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].serial);
    sm_memdev[sm_memdev_count-1].asset_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].asset);
    sm_memdev[sm_memdev_count-1].part_s = smbios_get_string(location, sm_memdev[sm_memdev_count-1].part);
 
 
    /*
    if(sm_memdev[sm_memdev_count-1].manufacturer_s){
    console_printf(get_vterm(0), "    manufacturer = %s \n", sm_memdev[sm_memdev_count-1].manufacturer_s);}
    
    if(sm_memdev[sm_memdev_count-1].size > 0 && sm_memdev[sm_memdev_count-1].size < 0xFFFF)
    {
        console_printf(get_vterm(0), "    size = %u", sm_memdev[sm_memdev_count-1].size);
        if(sm_memdev[sm_memdev_count-1].size)
        {console_printf(get_vterm(0), "MB\n");}
        else{console_printf(get_vterm(0), "KB\n");}
    }
    
    if(sm_memdev[sm_memdev_count-1].speed)
    {console_printf(get_vterm(0), "    speed = %uMHz\n", sm_memdev[sm_memdev_count-1].speed);}

    if(sm_memdev[sm_memdev_count-1].device_locator_s){
    console_printf(get_vterm(0), "    device = %s \n", sm_memdev[sm_memdev_count-1].device_locator_s);}   
    if(sm_memdev[sm_memdev_count-1].bank_locator_s){
    console_printf(get_vterm(0), "    bank = %s \n", sm_memdev[sm_memdev_count-1].bank_locator_s);}
    if(sm_memdev[sm_memdev_count-1].serial_s){
    console_printf(get_vterm(0), "    serial = %s \n", sm_memdev[sm_memdev_count-1].serial_s);}
    if(sm_memdev[sm_memdev_count-1].asset_s){
    console_printf(get_vterm(0), "    asset = %s \n", sm_memdev[sm_memdev_count-1].asset_s);}
    if(sm_memdev[sm_memdev_count-1].part_s){
    console_printf(get_vterm(0), "    part = %s \n", sm_memdev[sm_memdev_count-1].part_s);}
    */
}

static void parse_smbios_cpu(intptr_t location)
{
    memcpy((uint8_t*)&sm_cpu[sm_cpu_count-1], (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_cpu[sm_cpu_count-1].socket_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].socket);
    sm_cpu[sm_cpu_count-1].manufacturer_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].manufacturer);
    sm_cpu[sm_cpu_count-1].version_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].version);
    sm_cpu[sm_cpu_count-1].serial_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].serial);
    sm_cpu[sm_cpu_count-1].asset_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].asset);
    sm_cpu[sm_cpu_count-1].part_s = smbios_get_string(location, sm_cpu[sm_cpu_count-1].part);
 
    /*
    if(sm_cpu[sm_cpu_count-1].socket_s){
    console_printf(get_vterm(0), "    socket = %s \n", sm_cpu[sm_cpu_count-1].socket_s);}   
    if(sm_cpu[sm_cpu_count-1].manufacturer_s){
    console_printf(get_vterm(0), "    manufacturer = %s \n", sm_cpu[sm_cpu_count-1].manufacturer_s);}
    if(sm_cpu[sm_cpu_count-1].version_s){
    console_printf(get_vterm(0), "    version = %s \n", sm_cpu[sm_cpu_count-1].version_s);}
    if(sm_cpu[sm_cpu_count-1].serial_s){
    console_printf(get_vterm(0), "    serial = %s \n", sm_cpu[sm_cpu_count-1].serial_s);}
    if(sm_cpu[sm_cpu_count-1].asset_s){
    console_printf(get_vterm(0), "    asset = %s \n", sm_cpu[sm_cpu_count-1].asset_s);}
    if(sm_cpu[sm_cpu_count-1].part_s){
    console_printf(get_vterm(0), "    part = %s \n", sm_cpu[sm_cpu_count-1].part_s);}
    */
}

static void parse_smbios_chassis(intptr_t location)
{
    memcpy((uint8_t*)&sm_chas[sm_chas_count-1], (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_chas[sm_chas_count-1].manufacturer_s = smbios_get_string(location, sm_chas[sm_chas_count-1].manufacturer);
    sm_chas[sm_chas_count-1].version_s = smbios_get_string(location, sm_chas[sm_chas_count-1].version);
    sm_chas[sm_chas_count-1].serial_s = smbios_get_string(location, sm_chas[sm_chas_count-1].serial);
    sm_chas[sm_chas_count-1].asset_s = smbios_get_string(location, sm_chas[sm_chas_count-1].asset);
    
    /*
    if(sm_chas[sm_chas_count-1].manufacturer_s){
    console_printf(get_vterm(0), "    manufacturer = %s \n", sm_chas[sm_chas_count-1].manufacturer_s);}
    if(sm_chas[sm_chas_count-1].version_s){
    console_printf(get_vterm(0), "    version = %s \n", sm_chas[sm_chas_count-1].version_s);}
    if(sm_chas[sm_chas_count-1].serial_s){
    console_printf(get_vterm(0), "    serial = %s \n", sm_chas[sm_chas_count-1].serial_s);}
    if(sm_chas[sm_chas_count-1].asset_s){
    console_printf(get_vterm(0), "    asset = %s \n", sm_chas[sm_chas_count-1].asset_s);}
    */
}

static void parse_smbios_module(intptr_t location)
{
    memcpy((uint8_t*)&sm_mod[sm_mod_count-1], (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_mod[sm_mod_count-1].manufacturer_s = smbios_get_string(location, sm_mod[sm_mod_count-1].manufacturer);
    sm_mod[sm_mod_count-1].product_s = smbios_get_string(location, sm_mod[sm_mod_count-1].product);
    sm_mod[sm_mod_count-1].version_s = smbios_get_string(location, sm_mod[sm_mod_count-1].version);
    sm_mod[sm_mod_count-1].serial_s = smbios_get_string(location, sm_mod[sm_mod_count-1].serial);
    sm_mod[sm_mod_count-1].asset_s = smbios_get_string(location, sm_mod[sm_mod_count-1].asset);
    sm_mod[sm_mod_count-1].location_s = smbios_get_string(location, sm_mod[sm_mod_count-1].location);
    
    /*
    if(sm_mod[sm_mod_count-1].manufacturer_s){
    console_printf(get_vterm(0), "    manufacturer = %s \n", sm_mod[sm_mod_count-1].manufacturer_s);}
    if(sm_mod[sm_mod_count-1].product_s){
    console_printf(get_vterm(0), "    product = %s \n", sm_mod[sm_mod_count-1].product_s);}
    if(sm_mod[sm_mod_count-1].version_s){
    console_printf(get_vterm(0), "    version = %s \n", sm_mod[sm_mod_count-1].version_s);}
    if(sm_mod[sm_mod_count-1].serial_s){
    console_printf(get_vterm(0), "    serial = %s \n", sm_mod[sm_mod_count-1].serial_s);}
    if(sm_mod[sm_mod_count-1].asset_s){
    console_printf(get_vterm(0), "    asset = %s \n", sm_mod[sm_mod_count-1].asset_s);}
    if(sm_mod[sm_mod_count-1].location_s){
    console_printf(get_vterm(0), "    location = %s \n", sm_mod[sm_mod_count-1].location_s);}
    */
}

static void parse_smbios_system(intptr_t location)
{
    memcpy((uint8_t*)&sm_sys[sm_sys_count-1], (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_sys[sm_sys_count-1].manufacturer_s = smbios_get_string(location, sm_sys[sm_sys_count-1].manufacturer);
    sm_sys[sm_sys_count-1].product_s = smbios_get_string(location, sm_sys[sm_sys_count-1].product);
    sm_sys[sm_sys_count-1].version_s = smbios_get_string(location, sm_sys[sm_sys_count-1].version);
    sm_sys[sm_sys_count-1].serial_s = smbios_get_string(location, sm_sys[sm_sys_count-1].serial);
    sm_sys[sm_sys_count-1].sku_s = smbios_get_string(location, sm_sys[sm_sys_count-1].sku);
    sm_sys[sm_sys_count-1].family_s = smbios_get_string(location, sm_sys[sm_sys_count-1].family);
    
    /*
    if(sm_sys[sm_sys_count-1].manufacturer_s){
    console_printf(get_vterm(0), "    manufacturer = %s \n", sm_sys[sm_sys_count-1].manufacturer_s);}
    if(sm_sys[sm_sys_count-1].product_s){
    console_printf(get_vterm(0), "    product = %s \n", sm_sys[sm_sys_count-1].product_s);}
    if(sm_sys[sm_sys_count-1].version_s){
    console_printf(get_vterm(0), "    version = %s \n", sm_sys[sm_sys_count-1].version_s);}
    if(sm_sys[sm_sys_count-1].serial_s){
    console_printf(get_vterm(0), "    serial = %s \n", sm_sys[sm_sys_count-1].serial_s);}
    if(sm_sys[sm_sys_count-1].sku_s){
    console_printf(get_vterm(0), "    sku = %s \n", sm_sys[sm_sys_count-1].sku_s);}
    if(sm_sys[sm_sys_count-1].family_s){
    console_printf(get_vterm(0), "    family = %s \n", sm_sys[sm_sys_count-1].family_s);}
    */
}

static void parse_smbios_bios(intptr_t location)
{
    memcpy((uint8_t*)&sm_bios, (uint8_t*)(location + 4), sm_hdr.length - 4);
    
    location += sm_hdr.length;
    
    sm_bios.vendor_s = smbios_get_string(location, sm_bios.vendor);
    sm_bios.version_s = smbios_get_string(location, sm_bios.version);
    sm_bios.release_date_s = smbios_get_string(location, sm_bios.release_date);
    
    /*
    if(sm_bios.vendor_s){
    console_printf(get_vterm(0), "    vendor = %s \n", sm_bios.vendor_s);}
    if(sm_bios.version_s){
    console_printf(get_vterm(0), "    version = %s \n", sm_bios.version_s);}
    if(sm_bios.release_date_s){
    console_printf(get_vterm(0), "    release date = %s \n", sm_bios.release_date_s);}
    */
}

static void parse_structures(intptr_t location)
{
            
    sm_bios_count = sm_sys_count = 0;
    sm_mod_count = sm_chas_count = 0;
    sm_cpu_count = sm_memdev_count = 0;

    while(1)
    {
        sm_hdr.type = *(uint8_t*)location;
        sm_hdr.length = *(uint8_t*)(location + 1);
        sm_hdr.handle = *(uint16_t*)(location + 2);
        
        switch(sm_hdr.type)
        {
            case 0:
                // specs say that there should only be one BIOS structure
                //console_printf(get_vterm(0), "SMBIOS: BIOS: \n");
                if(sm_bios_count >= 1){break;}else{sm_bios_count++;}
                parse_smbios_bios(location);
                break;
                
            case 1:  
                //console_printf(get_vterm(0), "SMBIOS: SYSTEM: \n");
                if(sm_sys_count >= 4){break;}else{sm_sys_count++;}
                parse_smbios_system(location);
                break;
                
            case 2:  
                //console_printf(get_vterm(0), "SMBIOS: MODULE: \n");
                if(sm_mod_count >= 8){break;}else{sm_mod_count++;}
                parse_smbios_module(location);
                break;
                
            case 3:
                //console_printf(get_vterm(0), "SMBIOS: CHASSIS: \n");
                if(sm_chas_count >= 4){break;}else{sm_chas_count++;}
                parse_smbios_chassis(location);
                break;
                
            case 4:
                //console_printf(get_vterm(0), "SMBIOS: CPU: \n");
                if(sm_cpu_count >= 64){break;}else{sm_cpu_count++;}
                parse_smbios_cpu(location);
                break;
                
            case 17:
                //console_printf(get_vterm(0), "SMBIOS: MEMORY DEVICE: \n");
                if(sm_memdev_count >= 64){break;}else{sm_memdev_count++;}
                parse_smbios_memdev(location);
                break;
            
            case 127:
                return;
            
            default:
                //console_printf(get_vterm(0), "SMBIOS: structure(%u): \n", sm_hdr.type);
                break;
        }
        
        location += sm_hdr.length;
        while(*(uint16_t*)location++ != 0x0000){}
        location++;
    }
}

static void parse_eps(intptr_t location)
{
    memcpy((uint8_t*)&sm_eps, (uint8_t*)location, sizeof(sm_eps));
    
    if(smbios_checksum(location, sm_eps.length))
    {console_printf(get_vterm(0), "SMBIOS: EPS checksum is incorrect \n"); return;}
    
    if(sm_eps.major_version == 0x02 && sm_eps.minor_version == 0x01)
    {
        if(sm_eps.length == 0x1E)
        {console_printf(get_vterm(0), "SMBIOS: known bug found (sm_eps.length == 0x1E) \n");}
        else if(sm_eps.length != 0x1F)
        {console_printf(get_vterm(0), "SMBIOS: incorrect EPS length defined \n"); return;}
    }
    else
    {
        if(sm_eps.length != 0x1F)
        {console_printf(get_vterm(0), "SMBIOS: incorrect EPS length defined \n"); return;}
    }
    
    if(memcmp((uint8_t*)(location + 0x10), "_DMI_", 5))
    {console_printf(get_vterm(0), "SMBIOS: IEPS anchor string is incorrect \n"); return;}
    
    if(smbios_checksum(location + 0x10, 0x0F))
    {console_printf(get_vterm(0), "SMBIOS: IEPS checksum is incorrect \n"); return;}
    
    console_printf(get_vterm(0), "SMBIOS: version %u.%u \n", sm_eps.major_version, sm_eps.minor_version);
    
    parse_structures(sm_eps.struct_table_address);
}

void parse_smbios(void)
{
    intptr_t i;
    
    for(i = 0xF0000; i < 0x100000; i += 16)
    {
        if(!memcmp((uint8_t*)i, "_SM_", 4))
        {
            console_printf(get_vterm(0), "SMBIOS: EPS found at %x \n", i);
            parse_eps(i);
            return;
        }
    }
    
    console_printf(get_vterm(0), "SMBIOS: structure entry point was not found, skipping... \n");
}

