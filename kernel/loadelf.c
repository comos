#include "loadelf.h"
#include "elf.h"
#include "console.h"
#include "mm/paging.h"
#include "mm/region.h"
#include "align.h"
#include "task.h"

int load_elf_module(void *start_addr, intptr_t phys_addr, size_t length)
{
    struct Elf32_Ehdr *ehdr = start_addr;
    struct addr_space *aspace;
    struct Elf32_Phdr *phdr;
    int i;
    struct vm_region *vmr;

    // verify magic bytes
    if (ehdr->e_ident[EI_MAG0] == ELFMAG0
      && ehdr->e_ident[EI_MAG1] == ELFMAG1
      && ehdr->e_ident[EI_MAG2] == ELFMAG2
      && ehdr->e_ident[EI_MAG3] == ELFMAG3){
        // ok
    } else {
        trace("Not an ELF file.\n");
        return 1;
    }
    if (ehdr->e_ident[EI_CLASS] != ELFCLASS32){
        trace("invalid or unsupported ELF class\n");
        return 1;
    }
    if (ehdr->e_ident[EI_DATA] != ELFDATA2LSB){
        trace("invalid or unsupported ELF data encoding\n");
        return 1;
    }
    if (ehdr->e_type != ET_EXEC){
        trace("ELF file is not an executable. I can't, erm, execute it.\n");
        return 1;
    }
    if (ehdr->e_machine != EM_386){
        trace("ELF file is not for a 386. I can't execute it.\n");
        return 1;
    }
    // map program segments into the correct places
    aspace = addr_space_new();
    if (!aspace){
        trace("Cannot create addr_space for ELF module\n");
        return 1;
    }
    phdr = (struct Elf32_Phdr *) ((char *) start_addr + ehdr->e_phoff);
    for (i = 0; i < ehdr->e_phnum; ++i){
        if (phdr->p_type == PT_DYNAMIC){
            trace("ELF file requires dynamic linking, and I haven't written a dynamic linker yet.\n");
            addr_space_delete(aspace);
            return 1;
        } else if (phdr->p_type == PT_PHDR){
            trace("ELF file has a PT_PHDR segment; not implemented yet.\n");
            addr_space_delete(aspace);
            return 1;
        } else if (phdr->p_type == PT_LOAD){
            if (phdr->p_vaddr > 0xC0000000){
                trace("ELF file wants to load above 0xC0000000\n");
                addr_space_delete(aspace);
                return 1;
            } else if (phdr->p_vaddr < 0x1000){
                trace("ELF file wants to load in NULL page.\n");
                addr_space_delete(aspace);
                return 1;
            } else {
                // TODO: load bss segments: phdr->p_filesz == 0
                unsigned int map_flags = PTE_USER;
                if (phdr->p_flags & PF_W){
                    map_flags |= PTE_WRITABLE;
                }
                trace("Map segment phys(0x%.8X .. 0x%.8X) --> 0x%.8X\n",
                  phys_addr + phdr->p_offset,
                  phys_addr + phdr->p_offset + phdr->p_memsz,
                  phdr->p_vaddr);
                vmr = vm_region_new_physical_fixed(aspace,
                  phdr->p_vaddr,
                  phys_addr + phdr->p_offset,
                  uldivru(phdr->p_memsz, PAGE_SIZE));
                if (!vmr){
                    trace("pagedir_add_region() failed - cannot map ELF segment\n");
                }
            }
        } else {
            trace("Unknown segment type: %d!\n", phdr->p_type);
        }
        phdr = (struct Elf32_Phdr *) ((char *) phdr + ehdr->e_phentsize);
    }

    // create a task to run the module in
    create_task(aspace, // the address space we have just constructed
      (entry_point_t) ehdr->e_entry, // entry point
      "elfmodule", // task name!
      PRIO_NORMAL);
    return 0;
}
