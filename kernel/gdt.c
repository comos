#include "gdt.h"
#include "stddef.h"

// The Global Descriptor Table
// We only need a few hard-coded entries in this.
static unsigned char gdt[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // dummy (the CPU doesn't use gdt[0])
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9A, 0xCF, 0x00, // Code segment, DPL=0, 0x00000000..0xFFFFFFFF - SEG_DPL0_CODE
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x92, 0xCF, 0x00, // Data segment, DPL=0, 0x00000000..0xFFFFFFFF - SEG_DPL0_DATA
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFA, 0xCF, 0x00, // Code segment, DPL=3, 0x00000000..0xFFFFFFFF - SEG_DPL3_CODE
    0xFF, 0xFF, 0x00, 0x00, 0x00, 0xF2, 0xCF, 0x00, // Data segment, DPL=3, 0x00000000..0xFFFFFFFF - SEG_DPL3_DATA
    0x68, 0x00, 0xAA, 0xAA, 0xAA, 0x89, 0x00, 0xAA, // TSS descriptor (address is initialized to 0xAAAAAAAA,
                                                    // so it's easier to see if someting goes wrong)
};

extern char stack_end[]; // defined in kernel/start.asm

static struct tss tss0; // The default TSS. We'll need one for every CPU core.

// load task register with a task state segment descriptor
static void load_tss(int tss_seg_sel)
{
    asm volatile("ltr %%ax" :: "a" (tss_seg_sel));
}

// load data-segment registers
static void load_sreg(int data_seg_sel)
{
    asm volatile (
      "movw %%ax,%%ds \n"
      "movw %%ax,%%es \n"
      "movw %%ax,%%fs \n"
      "movw %%ax,%%gs \n"
      :: "a" (data_seg_sel));
}

// give the CPU the address of our GDT
static void load_gdt(void *gdt_address, size_t gdt_size)
{
    asm volatile (
      // put the address and size of the GDT on the stack,
      // in the format that the lgdt instruction wants it.
      "subl $6,%%esp        \n"
      "movw %%cx,(%%esp)    \n"
      "movl %%eax,2(%%esp)  \n"
      "lgdt (%%esp)         \n"
      "addl $6,%%esp        \n"
      :: "c" (gdt_size), "a" (gdt_address)
      : "cc");
}

// Main initialization function for the GDT
void gdt_init(void)
{
    // Set up the task state segment decriptor
    uint32_t tss_addr = (uint32_t) &tss0;
    gdt[0x2A] = tss_addr;
    gdt[0x2B] = tss_addr >> 8;
    gdt[0x2C] = tss_addr >> 16;
    gdt[0x2F] = tss_addr >> 24;

    // Tell the CPU where to find the GDT
    load_gdt(gdt, sizeof gdt);
    
    // Re-load all the data segment registers.
    // We load SEG_DPL3_DATA so that userspace code
    // will work too. Note that this is OK, security-wise,
    // because we're not using segmentation for protection.
    load_sreg(SEG_DPL3_DATA);

    // Load the task register
    load_tss(SEG_DPL3_TSS);

    // Set the important TSS fields
    tss0.ss0 = SEG_DPL0_DATA;
    tss0.esp0 = (uint32_t) stack_end; // XXX: eww!
}
