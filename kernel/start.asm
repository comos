bits 32
global _start           ; making entry point visible to linker
extern kmain            ; kmain is defined elsewhere
extern _text_start, _edata, _end  ; defined in linker script
extern _phys_base, _virt_base     ; defined in linker script

; setting up the Multiboot header - see GRUB docs for details
MODULEALIGN equ  1<<0                   ; align loaded modules on page boundaries
MEMINFO     equ  1<<1                   ; provide memory map
LOADINFO    equ  1<<16                  ; load addresses are provided
FLAGS       equ  MODULEALIGN | MEMINFO | LOADINFO  ; this is the Multiboot 'flag' field
MAGIC       equ    0x1BADB002           ; 'magic number' lets bootloader find the header
CHECKSUM    equ -(MAGIC + FLAGS)        ; checksum required

section multiboot
align 4
multiboot_header:
    dd MAGIC
    dd FLAGS
    dd CHECKSUM
    dd multiboot_header                 ; header_addr
    dd _text_start                      ; load_addr
    dd _edata                           ; load_end_addr
    dd _end                             ; bss_end_addr
    dd _start                           ; entry_addr

section startup

; reserve initial kernel stack space
STACKSIZE equ 0x4000                   ; that's 16k.

get_freemem_start:
    ; return the start address of free memory, after kernel image and modules
    ; mb_info is in ebx

    ; find the end of the kernel image, rounded up to the next page boundary
    mov   eax,_end
    test  eax,0xFFF   ; need to round up?
    jz    .noround
    add   eax,0x1000  ; round up
    and   eax,0xFFFFF000
.noround:
    push  eax         ; store end of kernel image

    ; get multiboot flags
    mov   edx,[ebx]
    test  edx,0x08    ; test MBI_MODS_XXX for modules support
    jz    .nomodules
    
    ; get mb_info fields
    mov   ecx,[ebx+20] ; mods_count
    mov   edx,[ebx+24] ; mods_addr

    ; empty modules list?
    test  ecx,ecx
    jz    .finish
.loop:
    ; loop through multiboot modules list
    mov   eax,[edx+4] ; mod_end
    test  eax,0xFFF   ; need to round up to nearest page?
    jz    .no_round_mod_end
    add   eax,0x1000  ; round mod_end up to page bourdany
    and   eax,0xFFFFF000
.no_round_mod_end:
    ; find highest mod_end we've found so far
    cmp   [esp],eax
    ja    .nextmod
    ; this one's the highest
    mov   [esp],eax   ; store the rounded mod_end
.nextmod:
    ; move on to next mb_module
    add   edx,byte 16
    dec   ecx
    jnz   .loop
.nomodules:
.finish:
    ; return highest address
    pop   eax
    ret

_start:
    mov   esp, stack+STACKSIZE ; set up the stack
    sub   esp,_virt_base       ; must use physical address for stack
    add   esp,_phys_base       ; until we enable paging!
    push  eax     ; pass Multiboot magic number
    push  ebx     ; pass Multiboot info structure
    cld           ; make sure direction flag is clear
                  ; (the calling convention is to keep it clear)

    ; find the start of free memory, after the kernel image and
    ; modules, in order to allocate space for page directory and
    ; page tables.
    call  get_freemem_start
    push  eax     ; push the address

    ; allocate a page directory
    add   eax,0x1000
    
    ; allocate two page tables
    push  eax
    add   eax,0x1000
    push  eax
    add   eax,0x1000
    push  eax

    ; clear the page directory
    mov   eax,[esp+12]
    mov   ecx,1024
.loop:
    mov   [eax],dword 0
    add   eax,byte 4
    dec   ecx
    jnz   .loop

    ; NOTE: we set the user bit in the page directory entries.
    ; This doesn't allow userspace code to read or write from/to
    ; the memory here, since it requires the user bit in the
    ; page table entry, too.

    ; point pagedir[0] -> one of our page tables
    ; this maps addresses 0x00000000 .. 0x00400000
    mov   eax,[esp+12] ; page directory
    mov   edx,[esp+8]  ; page table address
    or    edx,byte 7   ; present, writable, user
    mov   [eax],edx    ; put the entry in the table

    ; point pagedir[768] -> page table
    ; for addresses 0xC0000000 .. 0xC0400000
    mov   edx,[esp+4]
    or    edx,byte 7   ; present, writable, user
    mov   [eax+(768*4)],edx ; put the entry in the table

    ; identity map the first page table, so that when we enable
    ; paging, our kernel can keep on running (and not hit a
    ; brick wall) - when we enable paging, we can't jump at the
    ; same time, so if we don't map something to our current eip,
    ; we'll page-fault (and without an IDT, we'll double-fault and
    ; triple-fault, and the computer will restart.)

    xor   eax,eax      ; count page tables
    mov   ecx,[esp+8]  ; page table address
.loop2:
    mov   edx,eax
    shl   edx,12       ; (page table entry number) * (page size)
    or    edx,byte 3   ; present, writable, supervisor
    mov   [ecx],edx    ; put the entry in the table
    add   ecx,byte 4   ; move on to next entry
    inc   eax
    cmp   eax,1024
    jb    .loop2

    ; map the second page table:
    ; point 0xC0000000 .. 0xC0400000 to physical addresses 0x00100000 .. 0x00500000
    ; this should be enough for our entire kernel image (it's 4 mebibytes)
    
    xor   eax,eax
    mov   ecx,[esp+4]  ; address of our other page table
.loop3:
    mov   edx,eax
    shl   edx,12       ; (page table entry number) * (page size)
    add   edx,0x00100000
    or    edx,byte 3   ; present, writable, supervisor
    mov   [ecx],edx    ; put the entry in the table
    add   ecx,byte 4   ; move on to the next entry
    inc   eax
    cmp   eax,1024
    jb    .loop3

    ; now that we've set up our page tables and a page directory, we can tell the CPU
    ; where to find the page directory
    mov   eax,[esp+12]
    mov   cr3,eax

    ; and we can turn on paging
    mov   eax,cr0
    bts   eax,byte 31
    mov   cr0,eax
    ; we're still alive? Good.

    ; Store the start of free memory after the kernel image and modules,
    ; and now our page directory and page tables.
    mov   eax,[esp]
    add   esp,byte 16 ; pop our page table addresses - no longer needed
    push  eax         ; push the start of free mem

    ; reload the stack pointer to use virtual addresses > 0xC0000000
    ; our stuff on the stack will still be there, because it's mapped
    ; to the same physical address.
    mov   eax,esp
    sub   eax,_phys_base
    add   eax,_virt_base
    mov   esp,eax

    call  kmain   ; call kernel proper
    cli           ; halt machine should kernel return
    hlt

section .bss
align 32
global stack_end
stack:
    resb STACKSIZE               ; reserve 16k stack on a quadword boundary
stack_end:
