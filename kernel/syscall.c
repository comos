#include "syscall.h"
#include "interrupt.h"
#include "stddef.h"
#include "stdlib.h"
#include "console.h"

static syscall_t syscall_tab[SYSCALL_COUNT];

static void syscall_int(int vector, struct interrupt_stack *is)
{
    int syscall_number = is->r.eax;
    struct syscall_params sp;

    if (syscall_number < 0 || syscall_number >= SYSCALL_COUNT
      || syscall_tab[syscall_number] == NULL){
        trace("Bad syscall: %d\n", syscall_number);
        is->r.eax = -1;
        // TODO: set errno (somehow!)
    } else {
        sp.stack = (void *) is->esp;
        sp.a = is->r.ebx;
        sp.b = is->r.ecx;
        sp.c = is->r.edx;
        sp.d = is->r.esi;
        sp.e = is->r.edi;
        is->r.eax = syscall_tab[syscall_number](&sp);
    }
}

void syscall_init(void)
{
    interrupt_set_handler(0x80, syscall_int);
    interrupt_set_flags(0x80, INTR_USER);
    memset(syscall_tab, 0, sizeof syscall_tab);
}
