#include "chardev.h"
#include "stddef.h"
#include "console.h"

static struct chardev *devs[MAX_CHARDEV_MAJOR + 1];

void chardev_init(void)
{
    int i;
    for (i = 0; i <= MAX_CHARDEV_MAJOR; ++i){
        devs[i] = NULL;
    }
}

ssize_t chardev_read(struct chardev *d, void *buf, size_t len)
{
    return d->functab->read(d, buf, len);
}

ssize_t chardev_write(struct chardev *d, const void *buf, size_t len)
{
    return d->functab->write(d, buf, len);
}

int chardev_register(struct chardev *cdev)
{
    int major = cdev->major;
    if (devs[major]){
        devs[major]->prev = cdev;
    }

    cdev->prev = NULL;
    cdev->next = devs[major];
    devs[major] = cdev;
    return 0;
}

int chardev_unregister(struct chardev *cdev)
{
    int major = cdev->major;

    *(cdev->prev ? &cdev->prev->next : &devs[major])
      = cdev->next;

    if (cdev->next){
        cdev->next->prev = cdev->prev;
    }

    cdev->prev = cdev->next = NULL;
    return 0;
}

struct chardev *chardev_get(int major, int minor)
{
    struct chardev *cdev;
    cdev = devs[major];
    while (cdev && cdev->minor != minor){
        cdev = cdev->next;
    }
    return cdev;
}
