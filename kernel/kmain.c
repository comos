#include "align.h"
#include "console.h"
#include "gdt.h"
#include "cpuid.h"
#include "interrupt.h"
#include "keyboard.h"
#include "timer.h"
#include "multiboot.h"
#include "mp_tables.h"
#include "mm/mm.h"
#include "mm/paging.h"
#include "mm/region.h"
#include "general.h"
#include "serial.h"
#include "assert.h"
#include "critical.h"
#include "shell.h"
#include "task.h"
#include "chardev.h"
#include "loadelf.h"
#include "smbios.h"
#include "rtc.h"
#include "cpu.h"
#include "syscall.h"

static void read_mmap_info(struct mb_info *mbi)
{
    struct mb_mmap *region; // current memory region
    struct mb_mmap *end;    // end of memory region array
    region = (struct mb_mmap *) mbi->mmap_addr;
    end = (struct mb_mmap *) (mbi->mmap_addr + mbi->mmap_length);
    console_printf(get_vterm(0), "Memory map from bootloader, at address %.8X:\n", mbi->mmap_addr);
    console_puts(get_vterm(0),   "Range               Type\n");
    while (region < end){
        if (region->type == 1){
            // This is a free memory region
            console_set_fcolour(get_vterm(0), COLOR_BRIGHT_WHITE);
        } else {
            console_reset_colour(get_vterm(0));
        }
        console_printf(get_vterm(0), "%.8X - %.8X %d\n",
          (unsigned long) region->base_addr,
          (unsigned long) (region->base_addr + region->length - 1),
          region->type);
        if (region->type == 1){
            pmem_init_mark_free(region->base_addr, region->base_addr + region->length);
        }
        // locate next region
        region = (struct mb_mmap *) (((char *) region) + region->size + 4);
    }
    console_reset_colour(get_vterm(0));
}

static void read_multiboot_info(struct mb_info *mbi)
{
    if (mbi->flags & MBI_BOOT_LOADER_NAME){
        // Bootloader has given us a name. Why not print it?
        console_printf(get_vterm(0), "Thank you, %s.\n", mbi->boot_loader_name);
    }
    if (mbi->flags & MBI_MMAP_XXX){
        // read the memory map
        read_mmap_info(mbi);
    } else if (mbi->flags & MBI_MEM_XXX){
        // Use the more basic memory information
        console_printf(get_vterm(0), "Upper memory (mbi->mem_upper): %d kiB\n", mbi->mem_upper);
        pmem_init_mark_free(KERNEL_PHYS_BASE, KERNEL_PHYS_BASE + mbi->mem_upper * 1024);
    } else {
        panic("The bootloader gave us no memory information!");
    }
}

// ignore spurious irq 7
void spurious_int_7(int vector, struct interrupt_stack *is)
{
    //trace("suprious_int_7() called! \n");
    asm("pause;");
}

void read_mb_modules(struct mb_info *mbi)
{
    // XXX: we're assuming the first 4 MiB is identity-mapped!
    struct mb_module *mod;
    int count, i;
    if (mbi->flags & MBI_MODS_XXX){
        // find address of first module structure from the multiboot info
        mod = (struct mb_module *) mbi->mods_addr;
        count = mbi->mods_count;
        for (i=0; i<count; ++i){
            // print info about each module
            trace("Module: %s at 0x%.8X (%d kiB)\n", (char *) mod->string, mod->mod_start,
              uldivru(mod->mod_end - mod->mod_start, 1024));
            if (i < 1){
                // load only the first module, for now
                load_elf_module((void *)((KERNEL_VIRT_BASE - KERNEL_PHYS_BASE) + mod->mod_start), // start_addr
                  mod->mod_start, // phys_addr
                  mod->mod_end - mod->mod_start); // length
            }
            ++mod;
        }
        if (count == 0){
            trace("No multiboot modules found.\n");
        }
    } else {
        trace("No multiboot module information.\n");
        trace("Either the bootloader doesn't support modules,\n");
        trace("or no modules were loaded.\n");
    }
}

void kmain(uint32_t freemem_base, struct mb_info *mbi, unsigned int magic)
{
    MULTITASKING = false;
    chardev_init();
    console_init();

    // try initializing COM1 for 9600 baud output
    if (serial_init(&com1, 1, 9600) == 0){
        // also send console output to COM1
        console_serial_enable(get_vterm(0), &com1);
    }

    console_set_fcolour(get_vterm(0), COLOR_BRIGHT_WHITE);
    console_puts(get_vterm(0), "CommunityOS booting up...\n");
    console_reset_colour(get_vterm(0));
    if (magic != MB_BOOT_MAGIC){
        console_set_fcolour(get_vterm(0), COLOR_BRIGHT_RED);
        console_printf(get_vterm(0), "Bootloader gave us an invalid magic number: 0x%.8X\n", magic);
        console_reset_colour(get_vterm(0));
    }
    pmem_init_set_freemem_base(freemem_base);
    read_multiboot_info(mbi);
    console_printf(get_vterm(0), "Free memory: %d MiB\n", uldivru(pmem_get_size(), 1024 * 1024));
    mem_init();
    console_puts(get_vterm(0), "Loading GDT and task register...\n");
    gdt_init();
    console_puts(get_vterm(0), "Loading IDT...\n");
    interrupt_init();
    syscall_init();
    //console_puts(get_vterm(0), "Triggering interrupt 0x80 to test the IDT...\n");
    //asm volatile ("int $0x80" ::: "cc");
    console_puts(get_vterm(0), "Installing page fault handler...\n");
    set_pagefault_handler();
    paging_init();
    console_puts(get_vterm(0), "Configuring PIT...\n");
    timer_init();
    console_puts(get_vterm(0), "Getting CPUID information...\n");
    parse_cpuid();
    
    apic_init();
    enable_local_apic();
    init_sse();
    init_fpu();

    uint64_t counter;
    counter = read_cpu_timestamp();
    parse_mp_tables();
    counter = (read_cpu_timestamp() - counter);
    console_printf(get_vterm(0), "parse_mp_tables took 0x%llX ticks\n", counter);

    counter = read_cpu_timestamp();
    parse_smbios();
    counter = (read_cpu_timestamp() - counter);
    console_printf(get_vterm(0), "parse_smbios took 0x%llX ticks\n", counter);
    
    sti();
    console_puts(get_vterm(0), "Initializing Real-Time Clock...\n");
    init_rtc();
    
    console_puts(get_vterm(0), "Initializing keyboard driver...\n");
    keyboard_init();
    
    //console_set_fcolour(get_vterm(0), COLOR_BRIGHT_GREEN);
    //console_puts(get_vterm(0), "Bootup complete!\n"); no it's not!
    //console_reset_colour(get_vterm(0));
    interrupt_set_handler(irq_to_int(7), spurious_int_7);

    // Now, in order for the console code to continue working,
    // it needs 0xB8000 mapped somewhere sensible.
    // NOTE: the whole screen at 0xB8000 fits one page
    // (80 * 25 * 2 = 4000)
    intptr_t vram_vaddr;
    if (alloc_kvpages(1, 1, &vram_vaddr) < 1){
        panic("Cannot allocate (alloc_kvpages) virtual page for vram");
    }
    map_mem(0xB8000, vram_vaddr, 1, PTE_PRESENT | PTE_WRITABLE);
    // Tell the console driver where the new VRAM virtual-address is
    console_change_vram((uint16_t *) vram_vaddr);
    

#if 0
    {
        struct addr_space *aspace;
        struct vm_region *vm_r;
        aspace = addr_space_new();
        vm_r = vm_region_new_physical(aspace, 0x10000000, 1);
        addr_space_switch(aspace);
        asm volatile ("movl 0x10000800,%%eax" ::: "eax");
    }
#endif


    init_multitasking();
    read_mb_modules(mbi);
    
    //create_task((uint32_t)kmain_task, "Kernel Task", PRIO_HIGH);
	//create_task((uint32_t)kshell_task, "Kernel Shell", PRIO_NORMAL);
	
    sti();
    
    // enter an idle loop, so we can see the effects of interrupts
    for (;;){
        hlt();
    }
}

void kmain_task(void)
{
    critical_state_t crs = critical_enter();
    console_printf(get_vterm(0), "task_mgr: kmain_task() has been called \n");
    console_printf(get_vterm(1), "Hey, this is virtual terminal 1\n");

    {
        // test the device interface
        struct chardev *tty2 = chardev_get(4, 2);
        if (tty2){
            chardev_write(tty2, "Waay it's tty2!\n", 16);
        } else {
            trace("chardev_get(4, 2) failed\n");
        }
    }

    change_task_priority(active_id, PRIO_VERYLOW);
    critical_exit(crs);
    
    for (;;){
        hlt();
    }
}

