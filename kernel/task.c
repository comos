#include "task.h"
#include "general.h"
#include "console.h"
#include "stdlib.h"
#include "gdt.h"
#include "critical.h"
#include "cpustruct.h"
#include "timer.h"

// defined in task-switch.asm
extern void enter_task(struct pusha_struct *gpr, uint32_t eip, uint32_t eflags);

struct task_s task[MAX_TASKS];

struct task_s *create_task(struct addr_space *aspace, entry_point_t entry_pt, char name[TASK_NAME_LEN], uint32_t priority)
{
    struct task_s *task_s;

    // we need to be alone for this
    critical_state_t crs = critical_enter();
    
    uint32_t id;
    for (id = 0; id < MAX_TASKS; id++)
    {
        if (task[id].state == TASK_NULL || task[id].state == TASK_KILLED){
            break;
        }
    }
    
    /* error checking */
    if (id >= MAX_TASKS){
        trace("task_mgr: id >= MAX_TASKS\n");
        return NULL;
    }

    if (priority > 10){
        trace("task_mgr: priority > 10\n");
        return NULL;
    }

    if (priority == 0){
        trace("task_mgr: priority == 0\n");
        return NULL;
    }

    if (num_active_tasks >= MAX_TASKS){
        trace("task_mgr: too many tasks! ignoring create_task()\n");
        return NULL;
    }
    
    /* setup task structure */
    task_s = task + id;
    
    if(task_s->state != TASK_KILLED)
    {
        task_s->stack_base = malloc(2048);  /* allocate a 2KiB stack */
    }
    
    task_s->state = TASK_INIT;          /* set state to initializing */
    
    task_s->priority = priority;        /* set the priority */
    task_s->time_till_exec = task_s->priority; /* set time-till-execution to priority */
    
    strncpy(task_s->name, name, TASK_NAME_LEN - 1);    // copy name of task
    task_s->name[TASK_NAME_LEN - 1] = '\0';            // make sure it's null-terminated (strncmp
                                                       // won't null-terminate if the string was too long)
    
    task_s->aspace = aspace;
    task_s->gpr.esp = ((uint32_t) task_s->stack_base) + 2048;
    task_s->gpr.eax = 0;
    task_s->gpr.ecx = 0;
    task_s->gpr.edx = 0;
    task_s->gpr.ebx = 0;
    task_s->gpr.ebp = 0;
    task_s->gpr.esi = 0;
    task_s->gpr.edi = 0;
    task_s->eflags = 0x0200; // interrupts on, no other flags set
    task_s->eip = (uint32_t) entry_pt;
    
    num_active_tasks++;
    set_frequency(1000); /* set timer frequency */
    
    trace("task_mgr: creating task #%u (\"%s\")\n", id, task[id].name);
    task_s->state = TASK_READY;

    critical_exit(crs);
    return task_s;
}

void kill_task(uint32_t id)
{
    /* kernel-task (task #0) can NOT be killed unless it is the only task */
    if (id == 0 && num_active_tasks != 1){
        return;
    }

    /* we need to be alone for this code */
    critical_state_t crs = critical_enter();
    
    trace("task_mgr: killing task #%u (\"%s\")\n", id, task[id].name);
    
    /* "uninstall" the task (at least to the scheduler) */
    task[id].state = TASK_KILLED;

    /* remove the task name in case someone searches a task by name */
    memset((void *)task[id].name, 0, TASK_NAME_LEN);
    
    /* if the kernel-task is getting terminated, we need to go into a low-power 
       mode as nothing will really be running */
    if(id == 0)
    {
        console_printf(get_vterm(0), "task_mgr: no tasks are currently scheduled, entering low power mode\n");
        set_frequency(300); /* lower timer frequency to save power */
    }
    
    critical_exit(crs);
    
    for (;;){
        asm volatile ("hlt");
    }
}

void change_task_priority(uint32_t id, uint32_t priority)
{
    if(id >= MAX_TASKS)
    {trace("task_mgr: change_task_priority(%u, %u); id >= MAX_TASKS \n", id, priority); return;}
    if(priority > 10)
    {trace("task_mgr: change_task_priority(%u, %u); priority > 10 \n", id, priority);return;}
    if(!priority)
    {trace("task_mgr: change_task_priority(%u, %u); priority == 0 \n", id, priority); return;}
    if(id == 0 && priority < PRIO_VERYLOW)
    {
        trace("task_mgr: change_task_priority(%u, %u); \n", id, priority); 
        trace("        - the kernel's task priority may not be placed below PRIO_VERYLOW\n");
        return;
    }
    
    if(priority == PRIO_SLEEPING){num_sleeping_tasks++;}
    else if(task[id].priority == PRIO_SLEEPING && priority != PRIO_SLEEPING){num_sleeping_tasks--;}
    
    task[id].priority = priority;
}

uint32_t sched(void)
{
    uint32_t i;
    uint32_t lowest_priority = 10;  /* the lowest priority number */
    uint32_t lpt = 0; /* lowest priority task */
    uint32_t tasks_found = 0; /* number of active tasks found */
    
    /* scan through all the available task slots */
    for(i = 0; i < MAX_TASKS; i++)
    {   
        if(task[i].state == TASK_READY) /* is the task ready to execute? */
        {
            tasks_found++;
            
            if(lowest_priority >= task[i].time_till_exec) /* if the lowest priority is higher than this tasks */
            {                                             /* current time-till-execution, change the threshold */ 
                lowest_priority = task[i].time_till_exec; 
                lpt = i;                                 /* save this task id as the lowest priority task */
            }
            
            if(task[i].time_till_exec)
            {
                if(task[i].priority != PRIO_SLEEPING){
                task[i].time_till_exec--;   /* keep counting until it is ready to execute */
                }
            }   
            
            if(task[i].time_till_exec == 0) /* is this task ready to execute right now? */
            {
                task[i].time_till_exec = task[i].priority; /* reset the time-till-execution */
                active_id = i;
                return i;                                  /* return this task id */
            } 
            
            if(tasks_found >= num_active_tasks){break;}
        }
    }
    
    /* if no task was ready to execute above, execute the task with the 
       lowest current time-till-execution */
    if(task[lpt].state == TASK_READY)
    {
        active_id = lpt;
        return lpt;
    } 

    /* if we've made it here, either an invalid LPT was selected, or
       task[0] (kernel-task) is not ready to run, either way, it's an error */
    trace("task_mgr: no tasks!\n");

    for(;;){asm("hlt;");}
}

void switch_task(struct interrupt_stack *s)
{ 
    struct task_s *old_task, *new_task;
    uint32_t old_id = active_id;    
    
    /* get the next task's id */
    active_id = sched();

    old_task = task + old_id;
    new_task = task + active_id;

    /* neatly swap the task states over */
    if (old_id >= 0 && old_id < MAX_TASKS){
        old_task->gpr = s->r;
        old_task->eip = s->eip;
        old_task->eflags = s->eflags;
    }

    /* switching to a kernel thread means putting stuff
       on the thread's stack. We can't avoid this.
       We also really hope we don't end up overwriting
       our own stack! */
    addr_space_switch(new_task->aspace);
    enter_task(&new_task->gpr, new_task->eip, new_task->eflags);
}

void init_multitasking(void)
{
    uint32_t i;
    
    asm("cli;"); /* interrupts should be enabled, but let's be sure */
    for(i = 0; i < MAX_TASKS; i++){
        task[i].state = TASK_NULL;
    }
    
    active_id = -1;
    num_active_tasks = 0;
    num_sleeping_tasks = 0;
    MULTITASKING = true;
}
