bits 32
global _start

_start:
    ; fire lots of interrupts :D
loop1:
    mov   ecx,0xFFFFFF
loop2:
    ; waste some time
    nop
    nop
    nop
    xor   ecx,eax
    xor   eax,ecx
    xor   ecx,eax
    xor   eax,ecx
    xor   ecx,eax
    xor   eax,ecx
    
    dec   ecx
    jnz   loop2

    int   0x80
    jmp   loop1

